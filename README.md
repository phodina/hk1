# HK1 RBOX K8 Android box (RK3528)
This is an Android TV box with RK3528 SoC.

## Armbian
There are patches to run Armbian on the TV box
[rk3528-tvbox](https://github.com/ilyakurdyukov/rk3528-tvbox)

## Kernel version
```
$ uname -a
Linux localhost 5.10.157 #1 SMP PREEMPT Fri Sep 15 21:54:19 CST 2023 armv8l Toybox
```

## Kernel arguments
```
# cat /proc/cmdline
storagemedia=emmc androidboot.storagemedia=emmc androidboot.mode=normal  androidboot.dtb_idx=0 androidboot.dtbo_idx=0  androidboot.verifiedbootstate=orange androidboot.serialno=ebf3b3eb6130e910 console=ttyFIQ0 firmware_class.path=/vendor/etc/firmware init=/init rootwait ro loop.max_part=7 loglevel=3 androidboot.console=ttyFIQ0 androidboot.wificountrycode=SG androidboot.hardware=rk30board androidboot.boot_devices=ffbf0000.mmc androidboot.selinux=permissive buildvariant=user earlycon=uart8250,mmio32,0xff9f0000 driver_async_probe=dwmmc_rockchip,rockchip-drm drm_kms_helper.fbdev_emulation=0
```

## Kernel modules
```
# lsmod
Module                  Size  Used by
aic8800_fdrv          417792  0
rkvtunnel              40960  0
aic8800_bsp            73728  1 aic8800_fdrv
```

## Mount points
```
# mount
tmpfs on /dev type tmpfs (rw,seclabel,nosuid,relatime,size=1998232k,nr_inodes=499558,mode=755)
devpts on /dev/pts type devpts (rw,seclabel,relatime,mode=600,ptmxmode=000)
proc on /proc type proc (rw,relatime,gid=3009,hidepid=invisible)
sysfs on /sys type sysfs (rw,seclabel,relatime)
selinuxfs on /sys/fs/selinux type selinuxfs (rw,relatime)
tmpfs on /mnt type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=755,gid=1000)
/dev/block/mmcblk2p11 on /metadata type ext4 (rw,sync,seclabel,nosuid,nodev,noatime,discard)
/dev/block/dm-0 on / type ext4 (ro,seclabel,nodev,relatime,nobarrier)
/dev/block/dm-3 on /vendor type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-5 on /odm type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-1 on /system_dlkm type ext4 (ro,seclabel,relatime)
/dev/block/dm-2 on /system_ext type ext4 (ro,seclabel,relatime)
/dev/block/dm-4 on /vendor_dlkm type ext4 (ro,seclabel,relatime)
/dev/block/dm-6 on /odm_dlkm type ext4 (ro,seclabel,relatime)
/dev/block/dm-7 on /product type ext4 (ro,seclabel,relatime)
tmpfs on /apex type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=755)
tmpfs on /linkerconfig type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=755)
tmpfs on /mnt/installer type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=755,gid=1000)
tmpfs on /mnt/androidwritable type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=755,gid=1000)
none on /dev/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,blkio)
none on /sys/fs/cgroup type cgroup2 (rw,nosuid,nodev,noexec,relatime,memory_recursiveprot)
none on /dev/cpuctl type cgroup (rw,nosuid,nodev,noexec,relatime,cpu)
none on /dev/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,cpuset,noprefix,release_agent=/sbin/cpuset_release_agent)
none on /dev/memcg type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
tmpfs on /linkerconfig type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=755)
/dev/block/dm-0 on /apex/com.android.adbd type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.adservices type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.appsearch type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.art type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.btservices type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.conscrypt type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.extservices type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.i18n type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.ipsec type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.media type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.media.swcodec type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.mediaprovider type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.neuralnetworks type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.ondevicepersonalization type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.os.statsd type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.permission type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.resolv type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.runtime type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.scheduling type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.sdkext type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.tethering type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.tzdata type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.uwb type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.vndk.v33 type ext4 (ro,seclabel,relatime,nobarrier)
/dev/block/dm-0 on /apex/com.android.wifi type ext4 (ro,seclabel,relatime,nobarrier)
tracefs on /sys/kernel/tracing type tracefs (rw,seclabel,relatime,gid=3012,mode=755)
/sys/kernel/debug on /sys/kernel/debug type debugfs (rw,seclabel,relatime,mode=755)
/sys/kernel/debug/tracing on /sys/kernel/debug/tracing type tracefs (rw,seclabel,relatime,gid=3012,mode=755)
none on /config type configfs (rw,nosuid,nodev,noexec,relatime)
binder on /dev/binderfs type binder (rw,relatime,max=1048576,stats=global)
none on /sys/fs/fuse/connections type fusectl (rw,relatime)
none on /sys/fs/bpf type bpf (rw,nosuid,nodev,noexec,relatime)
pstore on /sys/fs/pstore type pstore (rw,seclabel,nosuid,nodev,noexec,relatime)
/dev/block/mmcblk2p10 on /cache type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,noauto_da_alloc)
tmpfs on /storage type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=755,gid=1000)
/dev/block/mmcblk2p14 on /data type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /data/user/0 type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
tmpfs on /data_mirror type tmpfs (rw,seclabel,nosuid,nodev,noexec,relatime,size=1998232k,nr_inodes=499558,mode=700,gid=1000)
/dev/block/mmcblk2p14 on /data_mirror/data_ce/null type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /data_mirror/data_ce/null/0 type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /data_mirror/data_de/null type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /data_mirror/misc_ce/null type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /data_mirror/misc_de/null type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /data_mirror/cur_profiles type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /data_mirror/ref_profiles type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
adb on /dev/usb-ffs/adb type functionfs (rw,relatime)
mtp on /dev/usb-ffs/mtp type functionfs (rw,relatime)
ptp on /dev/usb-ffs/ptp type functionfs (rw,relatime)
/dev/fuse on /mnt/user/0/emulated type fuse (rw,lazytime,nosuid,nodev,noexec,noatime,user_id=0,group_id=0,allow_other)
/dev/fuse on /mnt/installer/0/emulated type fuse (rw,lazytime,nosuid,nodev,noexec,noatime,user_id=0,group_id=0,allow_other)
/dev/fuse on /mnt/androidwritable/0/emulated type fuse (rw,lazytime,nosuid,nodev,noexec,noatime,user_id=0,group_id=0,allow_other)
/dev/fuse on /storage/emulated type fuse (rw,lazytime,nosuid,nodev,noexec,noatime,user_id=0,group_id=0,allow_other)
/dev/block/mmcblk2p14 on /mnt/pass_through/0/emulated type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /mnt/user/0/emulated/0/Android/data type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /storage/emulated/0/Android/data type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /mnt/androidwritable/0/emulated/0/Android/data type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /mnt/installer/0/emulated/0/Android/data type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /mnt/user/0/emulated/0/Android/obb type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /storage/emulated/0/Android/obb type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /mnt/androidwritable/0/emulated/0/Android/obb type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
/dev/block/mmcblk2p14 on /mnt/installer/0/emulated/0/Android/obb type ext4 (rw,seclabel,nosuid,nodev,noatime,nodiratime,discard,nodelalloc,resgid=1065)
```

## Dmesg
```
# dmesg
[    4.008618] Booting Linux on physical CPU 0x0000000000 [0x410fd034]
[    4.008638] Linux version 5.10.157 (user@epyc) (Android (8490178, based on r450784d) clang version 14.0.6 (https://android.googlesource.com/toolchain/llvm-project 4c603efb0cca074e9238af8b4106c30add4418f6), LLD 14.0.6) #1 SMP PREEMPT Fri Sep 15 21:54:19 CST 2023
[    4.013035] Machine model: Rockchip RK3528 DEMO1 LP4 V10 Board
[    4.049165] earlycon: uart8250 at MMIO32 0x00000000ff9f0000 (options '')
[    4.049177] printk: bootconsole [uart8250] enabled
[    4.051606] OF: fdt: Reserved memory: failed to reserve memory for node 'drm-cubic-lut@00000000': base 0x0000000000000000, size 0 MiB
[    4.051666] Reserved memory: created CMA memory pool at 0x00000000fb800000, size 8 MiB
[    4.051669] OF: reserved mem: initialized node cma, compatible id shared-dma-pool
[    4.104073] Zone ranges:
[    4.104084] DMA32    [mem 0x0000000000200000-0x00000000fbffffff]
[    4.104094] Normal   empty
[    4.104098] Movable zone start for each node
[    4.104101] Early memory node ranges
[    4.104105] node   0: [mem 0x0000000000200000-0x00000000083fffff]
[    4.104110] node   0: [mem 0x0000000008c00000-0x00000000fbffffff]
[    4.104115] Initmem setup node 0 [mem 0x0000000000200000-0x00000000fbffffff]
[    4.104121] On node 0 totalpages: 1029632
[    4.104127] DMA32 zone: 16120 pages used for memmap
[    4.104130] DMA32 zone: 0 pages reserved
[    4.104134] DMA32 zone: 1029632 pages, LIFO batch:63
[    4.130241] psci: probing for conduit method from DT.
[    4.130250] psci: PSCIv1.1 detected in firmware.
[    4.130253] psci: Using standard PSCI v0.2 function IDs
[    4.130258] psci: Trusted OS migration not required
[    4.130263] psci: SMC Calling Convention v1.2
[    4.130560] percpu: Embedded 32 pages/cpu s91480 r8192 d31400 u131072
[    4.130581] pcpu-alloc: s91480 r8192 d31400 u131072 alloc=32*4096
[    4.130588] pcpu-alloc: [0] 0 [0] 1 [0] 2 [0] 3
[    4.130656] Detected VIPT I-cache on CPU0
[    4.130683] CPU features: detected: ARM erratum 845719
[    4.130971] Built 1 zonelists, mobility grouping on.  Total pages: 1013512
[    4.130983] Kernel command line: storagemedia=emmc androidboot.storagemedia=emmc androidboot.mode=normal  androidboot.dtb_idx=0 androidboot.dtbo_idx=0  androidboot.verifiedbootstate=orange androidboot.serialno=ebf3b3eb6130e910 console=ttyFIQ0 firmware_class.path=/vendor/etc/firmware init=/init rootwait ro loop.max_part=7 loglevel=3 androidboot.console=ttyFIQ0 androidboot.wificountrycode=SG androidboot.hardware=rk30board androidboot.boot_devices=ffbf0000.mmc androidboot.selinux=permissive buildvariant=user earlycon=uart8250,mmio32,0xff9f0000 driver_async_probe=dwmmc_rockchip,rockchip-drm drm_kms_helper.fbdev_emulation=0
[    4.132125] Dentry cache hash table entries: 524288 (order: 10, 4194304 bytes, linear)
[    4.132513] Inode-cache hash table entries: 262144 (order: 9, 2097152 bytes, linear)
[    4.132618] mem auto-init: stack:all(zero), heap alloc:off, heap free:off
[    4.193383] Memory: 3986076K/4118528K available (18046K kernel code, 3746K rwdata, 7068K rodata, 960K init, 920K bss, 124260K reserved, 8192K cma-reserved)
[    4.193474] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
[    4.193697] rcu: Preemptible hierarchical RCU implementation.
[    4.193701] rcu: \x09RCU event tracing is enabled.
[    4.193705] rcu: \x09RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=4.
[    4.193709] \x09Trampoline variant of Tasks RCU enabled.
[    4.193712] \x09Tracing variant of Tasks RCU enabled.
[    4.193716] rcu: RCU calculated value of scheduler-enlistment delay is 30 jiffies.
[    4.193719] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
[    4.197798] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    4.198943] GIC: Using split EOI/Deactivate mode
[    4.199354] kfence: initialized - using 524288 bytes for 63 objects at 0x(____ptrval____)-0x(____ptrval____)
[    4.219536] arch_timer: cp15 timer(s) running at 24.00MHz (phys).
[    4.219551] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x588fe9dc0, max_idle_ns: 440795202592 ns
[    4.219560] sched_clock: 56 bits at 24MHz, resolution 41ns, wraps every 4398046511097ns
[    4.220284] Calibrating delay loop (skipped), value calculated using timer frequency.. 48.00 BogoMIPS (lpj=80000)
[    4.220298] pid_max: default: 32768 minimum: 301
[    4.220369] LSM: Security Framework initializing
[    4.220394] SELinux:  Initializing.
[    4.220458] Mount-cache hash table entries: 8192 (order: 4, 65536 bytes, linear)
[    4.220475] Mountpoint-cache hash table entries: 8192 (order: 4, 65536 bytes, linear)
[    4.222020] rcu: Hierarchical SRCU implementation.
[    4.223274] smp: Bringing up secondary CPUs ...
[    4.224446] Detected VIPT I-cache on CPU1
[    4.224500] CPU1: Booted secondary processor 0x0000000001 [0x410fd034]
[    4.225751] Detected VIPT I-cache on CPU2
[    4.225795] CPU2: Booted secondary processor 0x0000000002 [0x410fd034]
[    4.227111] Detected VIPT I-cache on CPU3
[    4.227155] CPU3: Booted secondary processor 0x0000000003 [0x410fd034]
[    4.227251] smp: Brought up 1 node, 4 CPUs
[    4.227258] SMP: Total of 4 processors activated.
[    4.227262] CPU features: detected: 32-bit EL0 Support
[    4.227266] CPU features: detected: CRC32 instructions
[    4.227310] CPU features: emulated: Privileged Access Never (PAN) using TTBR0_EL1 switching
[    4.236802] CPU: All CPU(s) started at EL2
[    4.236830] alternatives: patching kernel code
[    4.237821] devtmpfs: initialized
[    4.248757] Trying to unpack rootfs image as initramfs...
[    4.249046] Registered cp15_barrier emulation handler
[    4.249054] Registered setend emulation handler
[    4.249061] KASLR disabled due to lack of seed
[    4.249255] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 6370867519511994 ns
[    4.249266] futex hash table entries: 1024 (order: 4, 65536 bytes, linear)
[    4.249693] pinctrl core: initialized pinctrl subsystem
[    4.250705] NET: Registered protocol family 16
[    4.268759] DMA: preallocated 512 KiB GFP_KERNEL pool for atomic allocations
[    4.269105] DMA: preallocated 512 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
[    4.269189] audit: initializing netlink subsys (disabled)
[    4.269470] audit: type=2000 audit(0.046:1): state=initialized audit_enabled=0 res=1
[    4.271009] Registered FIQ tty driver
[    4.271280] thermal_sys: Registered thermal governor 'fair_share'
[    4.271285] thermal_sys: Registered thermal governor 'step_wise'
[    4.271288] thermal_sys: Registered thermal governor 'user_space'
[    4.271292] thermal_sys: Registered thermal governor 'power_allocator'
[    4.271581] cpuidle: using governor menu
[    4.271860] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
[    4.271971] ASID allocator initialised with 65536 entries
[    4.273674] ramoops: boot-log-0\x090x8000@0x0000000000110000
[    4.273704] ramoops: dmesg-0\x090x14000@0x0000000000118000
[    4.273708] ramoops: dmesg-1\x090x14000@0x000000000012c000
[    4.273722] ramoops: console\x090x80000@0x0000000000140000
[    4.273738] ramoops: pmsg\x090x30000@0x00000000001c0000
[    4.273806] printk: console [ramoops-1] enabled
[    4.274038] pstore: Registered ramoops as persistent store backend
[    4.274044] ramoops: using 0xe0000@0x110000, ecc: 0
[    4.288667] Freeing initrd memory: 1236K
[    4.295794] rockchip-gpio ff610000.gpio: probed /pinctrl/gpio@ff610000
[    4.296275] rockchip-gpio ffaf0000.gpio: probed /pinctrl/gpio@ffaf0000
[    4.296702] rockchip-gpio ffb00000.gpio: probed /pinctrl/gpio@ffb00000
[    4.297133] rockchip-gpio ffb10000.gpio: probed /pinctrl/gpio@ffb10000
[    4.297561] rockchip-gpio ffb20000.gpio: probed /pinctrl/gpio@ffb20000
[    4.297663] rockchip-pinctrl pinctrl: probed pinctrl
[    4.312790] fiq_debugger fiq_debugger.0: IRQ fiq not found
[    4.312803] fiq_debugger fiq_debugger.0: IRQ wakeup not found
[    4.312811] fiq_debugger_probe: could not install nmi irq handler
[    4.312882] printk: console [ttyFIQ0] enabled
[    4.312886] printk: bootconsole [uart8250] disabled
[    4.313050] Registered fiq debugger ttyFIQ0
[    4.313680] vcc5v0_sys: supplied by dc_12v
[    4.314051] vcc5v0_host: supplied by vcc5v0_sys
[    4.314293] vdd_0v9_s3: supplied by vcc5v0_sys
[    4.314518] vdd_1v8_s3: supplied by vcc5v0_sys
[    4.314739] vcc_3v3_s3: supplied by vcc5v0_sys
[    4.315001] vcc_ddr_s3: supplied by vcc5v0_sys
[    4.315214] vcc_sd: supplied by vcc_3v3_s3
[    4.315431] vccio_sd: supplied by vcc5v0_sys
[    4.315906] iommu: Default domain type: Translated
[    4.317680] SCSI subsystem initialized
[    4.317712] libata version 3.00 loaded.
[    4.317843] usbcore: registered new interface driver usbfs
[    4.317871] usbcore: registered new interface driver hub
[    4.317902] usbcore: registered new device driver usb
[    4.318254] mc: Linux media interface: v0.10
[    4.318275] videodev: Linux video capture interface: v2.00
[    4.318336] pps_core: LinuxPPS API ver. 1 registered
[    4.318339] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    4.318350] PTP clock support registered
[    4.318878] arm-scmi firmware:scmi: SCMI Notifications - Core Enabled.
[    4.318920] arm-scmi firmware:scmi: SCMI Protocol v2.0 'rockchip:' Firmware version 0x0
[    4.320477] Advanced Linux Sound Architecture Driver Initialized.
[    4.320839] Bluetooth: Core ver 2.22
[    4.320880] NET: Registered protocol family 31
[    4.320883] Bluetooth: HCI device and connection manager initialized
[    4.320891] Bluetooth: HCI socket layer initialized
[    4.320896] Bluetooth: L2CAP socket layer initialized
[    4.320908] Bluetooth: SCO socket layer initialized
[    4.321289] rockchip-cpuinfo cpuinfo: SoC\x09\x09: 35281000
[    4.321296] rockchip-cpuinfo cpuinfo: Serial\x09\x09: b15084082ea1d6b0
[    4.322062] clocksource: Switched to clocksource arch_sys_counter
[    4.393681] VFS: Disk quotas dquot_6.6.0
[    4.393729] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    4.394697] NET: Registered protocol family 2
[    4.394895] IP idents hash table entries: 65536 (order: 7, 524288 bytes, linear)
[    4.396472] tcp_listen_portaddr_hash hash table entries: 2048 (order: 3, 32768 bytes, linear)
[    4.396531] TCP established hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    4.396765] TCP bind hash table entries: 32768 (order: 7, 524288 bytes, linear)
[    4.397125] TCP: Hash tables configured (established 32768 bind 32768)
[    4.397271] UDP hash table entries: 2048 (order: 4, 65536 bytes, linear)
[    4.397340] UDP-Lite hash table entries: 2048 (order: 4, 65536 bytes, linear)
[    4.397516] NET: Registered protocol family 1
[    4.398002] PCI: CLS 0 bytes, default 64
[    4.399015] rockchip-thermal ffad0000.tsadc: tsadc is probed successfully!
[    4.400064] hw perfevents: enabled with armv8_cortex_a53 PMU driver, 7 counters available
[    4.401644] Initialise system trusted keyrings
[    4.401894] workingset: timestamp_bits=46 max_order=20 bucket_order=0
[    4.405338] utf8_selftest: All 154 tests passed
[    4.405984] Key type cifs.idmap registered
[    4.406001] ntfs: driver 2.1.32 [Flags: R/O].
[    4.406100] fuse: init (API version 7.32)
[    4.433036] Key type asymmetric registered
[    4.433044] Asymmetric key parser 'x509' registered
[    4.433096] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 242)
[    4.433101] io scheduler mq-deadline registered
[    4.433105] io scheduler kyber registered
[    4.433191] io scheduler bfq registered
[    4.435760] rockchip-usb2phy ffdf0000.usb2-phy: IRQ index 0 not found
[    4.440933] iep: Module initialized.
[    4.440996] mpp_service mpp-srv: unknown mpp version for missing VCS info
[    4.441001] mpp_service mpp-srv: probe start
[    4.442249] mpp_vdpu1 ff7c1000.avsd_plus: Adding to iommu group 2
[    4.442359] mpp_vdpu1 ff7c1000.avsd_plus: probe device
[    4.442559] mpp_vdpu1 ff7c1000.avsd_plus: reset_group->rw_sem_on=0
[    4.442569] mpp_vdpu1 ff7c1000.avsd_plus: reset_group->rw_sem_on=0
[    4.442852] mpp_vdpu1 ff7c1000.avsd_plus: probing finish
[    4.443435] mpp_vdpu2 ff7c0400.vdpu: Adding to iommu group 2
[    4.443488] mpp_vdpu2 ff7c0400.vdpu: probe device
[    4.443668] mpp_vdpu2 ff7c0400.vdpu: reset_group->rw_sem_on=0
[    4.443675] mpp_vdpu2 ff7c0400.vdpu: reset_group->rw_sem_on=0
[    4.443893] mpp_vdpu2 ff7c0400.vdpu: probing finish
[    4.444407] mpp-iep2 ff860000.iep: Adding to iommu group 5
[    4.444596] mpp-iep2 ff860000.iep: probe device
[    4.444798] mpp-iep2 ff860000.iep: reset_group->rw_sem_on=0
[    4.444808] mpp-iep2 ff860000.iep: reset_group->rw_sem_on=0
[    4.444817] mpp-iep2 ff860000.iep: reset_group->rw_sem_on=0
[    4.444861] mpp-iep2 ff860000.iep: allocate roi buffer failed
[    4.445077] mpp-iep2 ff860000.iep: probing finish
[    4.445508] mpp_jpgdec ff870000.jpegd: Adding to iommu group 6
[    4.445689] mpp_jpgdec ff870000.jpegd: probe device
[    4.446149] mpp_jpgdec ff870000.jpegd: probing finish
[    4.446558] mpp_rkvdec2 ff740100.rkvdec: Adding to iommu group 0
[    4.446799] mpp_rkvdec2 ff740100.rkvdec: rkvdec, probing start
[    4.446987] rkvdec2_init:996: failed on clk_get clk_core
[    4.446991] rkvdec2_init:999: failed on clk_get clk_cabac
[    4.447018] mpp_rkvdec2 ff740100.rkvdec: shared_niu_a is not found!
[    4.447022] rkvdec2_init:1022: No niu aclk reset resource define
[    4.447027] mpp_rkvdec2 ff740100.rkvdec: shared_niu_h is not found!
[    4.447030] rkvdec2_init:1025: No niu hclk reset resource define
[    4.447036] mpp_rkvdec2 ff740100.rkvdec: shared_video_core is not found!
[    4.447039] rkvdec2_init:1028: No core reset resource define
[    4.447044] mpp_rkvdec2 ff740100.rkvdec: shared_video_cabac is not found!
[    4.447047] rkvdec2_init:1031: No cabac reset resource define
[    4.447131] mpp_rkvdec2 ff740100.rkvdec: sram_start 0x00000000fe480000
[    4.447135] mpp_rkvdec2 ff740100.rkvdec: rcb_iova 0x0000000010000000
[    4.447140] mpp_rkvdec2 ff740100.rkvdec: sram_size 49152
[    4.447144] mpp_rkvdec2 ff740100.rkvdec: rcb_size 65536
[    4.447149] mpp_rkvdec2 ff740100.rkvdec: min_width 512
[    4.447201] mpp_rkvdec2 ff740100.rkvdec: link mode probe finish
[    4.447255] mpp_rkvdec2 ff740100.rkvdec: probing finish
[    4.447694] mpp_rkvenc2 ff780000.rkvenc: Adding to iommu group 1
[    4.447896] mpp_rkvenc2 ff780000.rkvenc: probing start
[    4.448158] mpp_rkvenc2 ff780000.rkvenc: dev_pm_opp_set_regulators: no regulator (venc) found: -19
[    4.448193] rkvenc_init:1814: failed to add venc devfreq
[    4.448477] mpp_rkvenc2 ff780000.rkvenc: probing finish
[    4.449368] mpp_vdpp ff861000.vdpp: Adding to iommu group 5
[    4.449415] mpp_vdpp ff861000.vdpp: probe device
[    4.449605] mpp_vdpp ff861000.vdpp: reset_group->rw_sem_on=0
[    4.449611] mpp_vdpp ff861000.vdpp: reset_group->rw_sem_on=0
[    4.449617] mpp_vdpp ff861000.vdpp: reset_group->rw_sem_on=0
[    4.449868] mpp_vdpp ff861000.vdpp: probing finish
[    4.450064] mpp_service mpp-srv: probe success
[    4.454974] dma-pl330 ffd60000.dma-controller: Loaded driver for PL330 DMAC-241330
[    4.454985] dma-pl330 ffd60000.dma-controller: \x09DBUFF-128x8bytes Num_Chans-8 Num_Peri-32 Num_Events-16
[    4.455448] rockchip-system-monitor rockchip-system-monitor: system monitor probe
[    4.455949] vdd_logic: supplied by vcc5v0_sys
[    4.456332] vdd_cpu: supplied by vcc5v0_sys
[    4.456577] arm-scmi firmware:scmi: Failed. SCMI protocol 22 not active.
[    4.456886] Serial: 8250/16550 driver, 10 ports, IRQ sharing disabled
[    4.457603] ffa00000.serial: ttyS2 at MMIO 0xffa00000 (irq = 42, base_baud = 1500000) is a 16550A
[    4.458186] ffa08000.serial: ttyS3 at MMIO 0xffa08000 (irq = 43, base_baud = 1500000) is a 16550A
[    4.459453] random: crng init done
[    4.460209] rockchip-vop2 ff840000.vop: Adding to iommu group 3
[    4.467006] cacheinfo: Unable to detect cache hierarchy for CPU 0
[    4.468233] rockchip-vop2 ff840000.vop: [drm:vop2_bind] vp0 assign plane mask: 0x0, primary plane phy id: -1
[    4.468245] rockchip-vop2 ff840000.vop: [drm:vop2_bind] vp1 assign plane mask: 0x0, primary plane phy id: -1
[    4.468617] rockchip-vop2 ff840000.vop: [drm:vop2_create_crtc] Esmart2-win0 as cursor plane for vp0
[    4.468702] [drm:rockchip_drm_get_dclk_by_width] *ERROR* Can't not find 720 width solution and use 148500 khz as max dclk
[    4.469077] [drm] unsupported AFBC format[3231564e]
[    4.469275] rockchip-drm display-subsystem: bound ff840000.vop (ops vop2_component_ops)
[    4.469943] dwhdmi-rockchip ff8d0000.hdmi: Detected HDMI TX controller v2.11a with HDCP (inno_dw_hdmi_phy2)
[    4.470468] dwhdmi-rockchip ff8d0000.hdmi: registered DesignWare HDMI I2C bus driver
[    4.471752] input: hdmi_cec_key as /devices/platform/ff8d0000.hdmi/dw-hdmi-cec.2.auto/input/input0
[    4.472482] rockchip-drm display-subsystem: bound ff8d0000.hdmi (ops dw_hdmi_rockchip_ops)
[    4.472746] rockchip-drm display-subsystem: bound ff880000.tve (ops rockchip_tve_component_ops)
[    4.473518] rockchip-drm display-subsystem: route-hdmi: failed to get logo,offset
[    4.475481] brd: module loaded
[    4.481890] [drm] Initialized rockchip 3.0.0 20140818 for display-subsystem on minor 0
[    4.486080] loop: module loaded
[    4.486430] zram: Added device: zram0
[    4.487227] SCSI Media Changer driver v0.25
[    4.489563] tun: Universal TUN/TAP device driver, 1.6
[    4.490026] rk_gmac-dwmac ffbd0000.ethernet: IRQ eth_lpi not found
[    4.490199] rk_gmac-dwmac ffbd0000.ethernet: PTP uses main clock
[    4.490235] rk_gmac-dwmac ffbd0000.ethernet: no regulator found
[    4.490240] rk_gmac-dwmac ffbd0000.ethernet: clock input or output? (input).
[    4.490245] rk_gmac-dwmac ffbd0000.ethernet: Can not read property: tx_delay.
[    4.490249] rk_gmac-dwmac ffbd0000.ethernet: set tx_delay to 0xffffffff
[    4.490254] rk_gmac-dwmac ffbd0000.ethernet: Can not read property: rx_delay.
[    4.490258] rk_gmac-dwmac ffbd0000.ethernet: set rx_delay to 0xffffffff
[    4.490305] rk_gmac-dwmac ffbd0000.ethernet: integrated PHY? (yes).
[    4.490334] rk_gmac-dwmac ffbd0000.ethernet: cannot get clock clk_mac_speed
[    4.490338] rk_gmac-dwmac ffbd0000.ethernet: clock input from PHY
[    4.490583] rk_gmac-dwmac ffbd0000.ethernet: init for RMII
[    4.490771] rk_gmac-dwmac ffbd0000.ethernet: User ID: 0x30, Synopsys ID: 0x51
[    4.490778] rk_gmac-dwmac ffbd0000.ethernet: \x09DWMAC4/5
[    4.490784] rk_gmac-dwmac ffbd0000.ethernet: DMA HW capability register supported
[    4.490788] rk_gmac-dwmac ffbd0000.ethernet: RX Checksum Offload Engine supported
[    4.490792] rk_gmac-dwmac ffbd0000.ethernet: TX Checksum insertion supported
[    4.490796] rk_gmac-dwmac ffbd0000.ethernet: Wake-Up On Lan supported
[    4.490859] rk_gmac-dwmac ffbd0000.ethernet: TSO supported
[    4.490864] rk_gmac-dwmac ffbd0000.ethernet: Enable RX Mitigation via HW Watchdog Timer
[    4.490869] rk_gmac-dwmac ffbd0000.ethernet: TSO feature enabled
[    4.490874] rk_gmac-dwmac ffbd0000.ethernet: Using 40 bits DMA width
[    4.492330] PPP generic driver version 2.4.2
[    4.492511] PPP BSD Compression module registered
[    4.492516] PPP Deflate Compression module registered
[    4.492536] PPP MPPE Compression module registered
[    4.492540] NET: Registered protocol family 24
[    4.492560] PPTP driver version 0.8.5
[    4.492788] usbcore: registered new interface driver catc
[    4.492812] usbcore: registered new interface driver kaweth
[    4.492815] pegasus: v0.9.3 (2013/04/25), Pegasus/Pegasus II USB Ethernet driver
[    4.492836] usbcore: registered new interface driver pegasus
[    4.492857] usbcore: registered new interface driver rtl8150
[    4.492890] usbcore: registered new interface driver r8152
[    4.492893] hso: drivers/net/usb/hso.c: Option Wireless
[    4.492935] usbcore: registered new interface driver hso
[    4.492960] usbcore: registered new interface driver asix
[    4.492984] usbcore: registered new interface driver ax88179_178a
[    4.493005] usbcore: registered new interface driver cdc_ether
[    4.493027] usbcore: registered new interface driver cdc_eem
[    4.493048] usbcore: registered new interface driver dm9601
[    4.493075] usbcore: registered new interface driver smsc75xx
[    4.493105] usbcore: registered new interface driver smsc95xx
[    4.493126] usbcore: registered new interface driver gl620a
[    4.493147] usbcore: registered new interface driver net1080
[    4.493169] usbcore: registered new interface driver plusb
[    4.493193] usbcore: registered new interface driver rndis_host
[    4.493215] usbcore: registered new interface driver cdc_subset
[    4.493237] usbcore: registered new interface driver zaurus
[    4.493260] usbcore: registered new interface driver MOSCHIP usb-ethernet driver
[    4.493285] usbcore: registered new interface driver int51x1
[    4.493310] usbcore: registered new interface driver kalmia
[    4.493332] usbcore: registered new interface driver ipheth
[    4.493355] usbcore: registered new interface driver sierra_net
[    4.493377] usbcore: registered new interface driver cx82310_eth
[    4.493408] usbcore: registered new interface driver cdc_ncm
[    4.493430] usbcore: registered new interface driver qmi_wwan
[    4.493451] usbcore: registered new interface driver cdc_mbim
[    4.499820] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    4.499829] ehci-pci: EHCI PCI platform driver
[    4.499892] ehci-platform: EHCI generic platform driver
[    4.502107] ehci-platform ff100000.usb: EHCI Host Controller
[    4.502286] ehci-platform ff100000.usb: new USB bus registered, assigned bus number 1
[    4.502379] ehci-platform ff100000.usb: irq 19, io mem 0xff100000
[    4.515404] ehci-platform ff100000.usb: USB 2.0 started, EHCI 1.00
[    4.515535] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.10
[    4.515541] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    4.515546] usb usb1: Product: EHCI Host Controller
[    4.515550] usb usb1: Manufacturer: Linux 5.10.157 ehci_hcd
[    4.515554] usb usb1: SerialNumber: ff100000.usb
[    4.515952] hub 1-0:1.0: USB hub found
[    4.515979] hub 1-0:1.0: 1 port detected
[    4.516537] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    4.516569] ohci-platform: OHCI generic platform driver
[    4.516849] ohci-platform ff140000.usb: Generic Platform OHCI controller
[    4.517034] ohci-platform ff140000.usb: new USB bus registered, assigned bus number 2
[    4.517128] ohci-platform ff140000.usb: irq 20, io mem 0xff140000
[    4.576197] usb usb2: New USB device found, idVendor=1d6b, idProduct=0001, bcdDevice= 5.10
[    4.576204] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    4.576209] usb usb2: Product: Generic Platform OHCI controller
[    4.576213] usb usb2: Manufacturer: Linux 5.10.157 ohci_hcd
[    4.576218] usb usb2: SerialNumber: ff140000.usb
[    4.576639] hub 2-0:1.0: USB hub found
[    4.576665] hub 2-0:1.0: 1 port detected
[    4.577616] xhci-hcd xhci-hcd.4.auto: xHCI Host Controller
[    4.577812] xhci-hcd xhci-hcd.4.auto: new USB bus registered, assigned bus number 3
[    4.577960] xhci-hcd xhci-hcd.4.auto: hcc params 0x0220fe64 hci version 0x110 quirks 0x0000202002010010
[    4.578003] xhci-hcd xhci-hcd.4.auto: irq 77, io mem 0xfe500000
[    4.578262] usb usb3: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.10
[    4.578267] usb usb3: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    4.578272] usb usb3: Product: xHCI Host Controller
[    4.578276] usb usb3: Manufacturer: Linux 5.10.157 xhci-hcd
[    4.578280] usb usb3: SerialNumber: xhci-hcd.4.auto
[    4.578695] hub 3-0:1.0: USB hub found
[    4.578749] hub 3-0:1.0: 1 port detected
[    4.578999] xhci-hcd xhci-hcd.4.auto: xHCI Host Controller
[    4.579163] xhci-hcd xhci-hcd.4.auto: new USB bus registered, assigned bus number 4
[    4.579177] xhci-hcd xhci-hcd.4.auto: Host supports USB 3.0 SuperSpeed
[    4.579228] usb usb4: We don't know the algorithms for LPM for this host, disabling LPM.
[    4.579323] usb usb4: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.10
[    4.579328] usb usb4: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    4.579333] usb usb4: Product: xHCI Host Controller
[    4.579337] usb usb4: Manufacturer: Linux 5.10.157 xhci-hcd
[    4.579341] usb usb4: SerialNumber: xhci-hcd.4.auto
[    4.579706] hub 4-0:1.0: USB hub found
[    4.579731] hub 4-0:1.0: 1 port detected
[    4.580091] usbcore: registered new interface driver cdc_acm
[    4.580096] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[    4.580137] usbcore: registered new interface driver usblp
[    4.580165] usbcore: registered new interface driver cdc_wdm
[    4.580424] usbcore: registered new interface driver uas
[    4.580485] usbcore: registered new interface driver usb-storage
[    4.580515] usbcore: registered new interface driver ums-alauda
[    4.580539] usbcore: registered new interface driver ums-cypress
[    4.580562] usbcore: registered new interface driver ums-datafab
[    4.580584] usbcore: registered new interface driver ums_eneub6250
[    4.580605] usbcore: registered new interface driver ums-freecom
[    4.580629] usbcore: registered new interface driver ums-isd200
[    4.580651] usbcore: registered new interface driver ums-jumpshot
[    4.580674] usbcore: registered new interface driver ums-karma
[    4.580699] usbcore: registered new interface driver ums-onetouch
[    4.580722] usbcore: registered new interface driver ums-sddr09
[    4.580747] usbcore: registered new interface driver ums-sddr55
[    4.580770] usbcore: registered new interface driver ums-usbat
[    4.580835] usbcore: registered new interface driver usbserial_generic
[    4.580851] usbserial: USB Serial support registered for generic
[    4.580878] usbcore: registered new interface driver option
[    4.580893] usbserial: USB Serial support registered for GSM modem (1-port)
[    4.580989] usbcore: registered new interface driver trancevibrator
[    4.581827] usbcore: registered new interface driver xpad
[    4.581875] usbcore: registered new interface driver usb_acecad
[    4.581902] usbcore: registered new interface driver aiptek
[    4.581928] usbcore: registered new interface driver gtco
[    4.581952] usbcore: registered new interface driver hanwang
[    4.581978] usbcore: registered new interface driver kbtab
[    4.582308] .. rk pwm remotectl v2.0 init
[    4.582540] input: ffa90030.pwm as /devices/platform/ffa90030.pwm/input/input1
[    4.582826] remotectl-pwm ffa90030.pwm: pwm version is 0x3150000
[    4.582862] remotectl-pwm ffa90030.pwm: Controller support pwrkey capture
[    4.583139] i2c /dev entries driver
[    4.584836] usbcore: registered new interface driver uvcvideo
[    4.584843] USB Video Class driver (1.1.1)
[    4.585629] test_power_init: could not find dev node
[    4.586124] device-mapper: uevent: version 1.0.3
[    4.586401] device-mapper: ioctl: 4.44.0-ioctl (2021-02-01) initialised: dm-devel@redhat.com
[    4.586605] Bluetooth: HCI UART driver ver 2.3
[    4.586611] Bluetooth: HCI UART protocol H4 registered
[    4.587025] cpu cpu0: leakage=17
[    4.588588] cpu cpu0: pvtm=1469
[    4.589038] cpu cpu0: pvtm-volt-sel=5
[    4.590148] cpu cpu0: avs=0
[    4.590241] cpu cpu0: EM: OPP:816000 is inefficient
[    4.590245] cpu cpu0: EM: OPP:408000 is inefficient
[    4.590430] cpu cpu0: EM: created perf domain
[    4.590475] cpu cpu0: l=-2147483648 h=2147483647 hyst=0 l_limit=0 h_limit=0 h_table=0
[    4.592902] sdhci: Secure Digital Host Controller Interface driver
[    4.592910] sdhci: Copyright(c) Pierre Ossman
[    4.592913] Synopsys Designware Multimedia Card Interface Driver
[    4.593317] sdhci-pltfm: SDHCI platform and OF driver helper
[    4.593968] dwmmc_rockchip ffc20000.mmc: IDMAC supports 32-bit address mode.
[    4.593977] dwmmc_rockchip ffc30000.mmc: IDMAC supports 32-bit address mode.
[    4.593996] dwmmc_rockchip ffc20000.mmc: Using internal DMA controller.
[    4.593998] dwmmc_rockchip ffc30000.mmc: Using internal DMA controller.
[    4.594007] dwmmc_rockchip ffc30000.mmc: Version ID is 270a
[    4.594009] dwmmc_rockchip ffc20000.mmc: Version ID is 270a
[    4.594056] dwmmc_rockchip ffc30000.mmc: DW MMC controller at irq 54,32 bit host data width,256 deep fifo
[    4.594069] dwmmc_rockchip ffc20000.mmc: DW MMC controller at irq 53,32 bit host data width,256 deep fifo
[    4.594643] dwmmc_rockchip ffc20000.mmc: allocated mmc-pwrseq
[    4.594660] mmc_host mmc1: card is non-removable.
[    4.595249] FD628: Fd628 Driver init.
[    4.595650] FD628: fd628_driver_probe get in
[    4.595911] FD628: register_fd628_driver: Successed to register_fd628_driver
[    4.596869] arm-scmi firmware:scmi: Failed. SCMI protocol 17 not active.
[    4.596927] SMCCC: SOC_ID: ARCH_SOC_ID not implemented, skipping ....
[    4.597358] rk-crypto ffc40000.crypto: invalid resource
[    4.598374] rk-crypto ffc40000.crypto: register to cryptodev ok!
[    4.598390] rk-crypto ffc40000.crypto: CRYPTO V3.0.0.0 multi Accelerator successfully registered
[    4.599028] cryptodev: driver 1.12 loaded.
[    4.599081] hid: raw HID events driver (C) Jiri Kosina
[    4.601321] usbcore: registered new interface driver usbhid
[    4.601329] usbhid: USB HID core driver
[    4.601621] ashmem: initialized
[    4.602155] rockchip,bus gpu-bus: leakage=23
[    4.602168] rockchip,bus gpu-bus: leakage-volt-sel=1
[    4.602375] rockchip,bus gpu-bus: avs=0
[    4.602905] rockchip-dmc dmc: failed to get the count of devfreq-event in /dmc node
[    4.603096] rockchip-dmc dmc: leakage=23
[    4.603107] rockchip-dmc dmc: leakage-volt-sel=3
[    4.603325] rockchip-dmc dmc: avs=0
[    4.603338] rockchip-dmc dmc: failed to get vop bandwidth to dmc rate
[    4.603341] rockchip-dmc dmc: failed to get vop pn to msch rl
[    4.603345] rockchip-dmc dmc: don't add devfreq feature
[    4.603363] rockchip-dmc dmc: l=10000 h=2147483647 hyst=5000 l_limit=0 h_limit=0 h_table=0
[    4.603423] rockchip-dmc dmc: Get wrong frequency, Request 920000000, Current 786000000
[    4.605134] optee: probing for conduit method.
[    4.605157] optee: revision 3.13 (c73fd553)
[    4.605357] optee: dynamic shared memory is enabled
[    4.605529] optee: initialized driver
[    4.605629] mmc_host mmc0: Bus speed (slot 0) = 400000Hz (slot req 400000Hz, actual 400000HZ div = 0)
[    4.605844] rknandbase v1.2 2021-01-07
[    4.607096] usbcore: registered new interface driver snd-usb-audio
[    4.607586] mmc_host mmc1: Bus speed (slot 0) = 400000Hz (slot req 400000Hz, actual 400000HZ div = 0)
[    4.608205] rk3528-acodec ffe10000.acodec: Use pa_ctl_gpio and pa_ctl_delay_ms: 0
[    4.612548] input: rockchip,hdmi rockchip,hdmi as /devices/platform/hdmi-sound/sound/card0/input2
[    4.613319] netem: version 1.3
[    4.613327] u32 classifier
[    4.613329] input device check on
[    4.613331] Actions configured
[    4.614076] xt_time: kernel timezone is -0000
[    4.614171] gre: GRE over IPv4 demultiplexor driver
[    4.614174] IPv4 over IPsec tunneling driver
[    4.614710] Initializing XFRM netlink socket
[    4.614735] IPsec XFRM device driver
[    4.614972] NET: Registered protocol family 10
[    4.615921] Segment Routing with IPv6
[    4.615994] mip6: Mobile IPv6
[    4.616549] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    4.617107] NET: Registered protocol family 17
[    4.617127] NET: Registered protocol family 15
[    4.617175] Bridge firewalling registered
[    4.617441] Bluetooth: RFCOMM TTY layer initialized
[    4.617456] Bluetooth: RFCOMM socket layer initialized
[    4.617487] Bluetooth: RFCOMM ver 1.11
[    4.617509] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[    4.617511] Bluetooth: BNEP filters: protocol multicast
[    4.617517] Bluetooth: BNEP socket layer initialized
[    4.617520] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    4.617524] Bluetooth: HIDP socket layer initialized
[    4.617546] l2tp_core: L2TP core driver, V2.0
[    4.617554] l2tp_ppp: PPPoL2TP kernel driver, V2.0
[    4.617556] [BT_RFKILL]: Enter rfkill_rk_init
[    4.617559] [WLAN_RFKILL]: Enter rfkill_wlan_init
[    4.618089] [WLAN_RFKILL]: Enter rfkill_wlan_probe
[    4.618128] [WLAN_RFKILL]: wlan_platdata_parse_dt: wifi_chip_type = rtl8822cs
[    4.618131] [WLAN_RFKILL]: wlan_platdata_parse_dt: enable wifi power control.
[    4.618134] [WLAN_RFKILL]: wlan_platdata_parse_dt: wifi power controled by gpio.
[    4.618151] [WLAN_RFKILL]: wlan_platdata_parse_dt: WIFI,poweren_gpio = 106 flags = 0.
[    4.618168] [WLAN_RFKILL]: wlan_platdata_parse_dt: WIFI,host_wake_irq = 107, flags = 0.
[    4.618175] [WLAN_RFKILL]: wlan_platdata_parse_dt: The ref_wifi_clk not found !
[    4.618177] [WLAN_RFKILL]: rfkill_wlan_probe: init gpio
[    4.618182] [WLAN_RFKILL]: rfkill_set_wifi_bt_power: 1
[    4.618187] [WLAN_RFKILL]: Exit rfkill_wlan_probe
[    4.618848] [BT_RFKILL]: bluetooth_platdata_parse_dt: get property: uart_rts_gpios = 98.
[    4.618873] [BT_RFKILL]: bluetooth_platdata_parse_dt: get property: BT,reset_gpio = 114.
[    4.618885] [BT_RFKILL]: bluetooth_platdata_parse_dt: get property: BT,wake_host_irq = 113.
[    4.618892] [BT_RFKILL]: bluetooth_platdata_parse_dt: clk_get failed!!!.
[    4.618934] [BT_RFKILL]: Request irq for bt wakeup host
[    4.619024] [BT_RFKILL]: ** disable irq
[    4.619185] [BT_RFKILL]: bt_default device registered.
[    4.619796] registered taskstats version 1
[    4.619808] Loading compiled-in X.509 certificates
[    4.619926] Key type .fscrypt registered
[    4.619930] Key type fscrypt-provisioning registered
[    4.620152] pstore: Using crash dump compression: deflate
[    4.620842] rga2 ff850000.rga: Adding to iommu group 4
[    4.620960] rga: rga2, irq = 35, match scheduler
[    4.621323] rga: rga2 hardware loaded successfully, hw_version:3.7.93215.
[    4.621367] rga: rga2 probe successfully
[    4.621538] rga_iommu: IOMMU binding successfully, default mapping core[0x4]
[    4.621747] rga: Module initialized. v1.2.25
[    4.624720] mmc2: SDHCI controller on ffbf0000.mmc [ffbf0000.mmc] using ADMA
[    4.640741] input: adc-keys as /devices/platform/adc-keys/input/input3
[    4.689210] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[    4.690410] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[    4.690556] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[    4.690566] cfg80211: failed to load regulatory.db
[    4.691300] rockchip-suspend not set sleep-mode-config for mem-lite
[    4.691309] rockchip-suspend not set wakeup-config for mem-lite
[    4.691317] rockchip-suspend not set sleep-mode-config for mem-ultra
[    4.691320] rockchip-suspend not set wakeup-config for mem-ultra
[    4.692310] I : [File] : drivers/gpu/arm/mali400/mali/linux/mali_kernel_linux.c; [Line] : 410; [Func] : mali_module_init(); svn_rev_string_from_arm of this mali_ko is '', rk_ko_ver is '5', built at '21:54:36', on 'Sep 15 2023'.
[    4.692681] mali-utgard ff700000.gpu: mali_platform_device->num_resources = 8
[    4.692691] mali-utgard ff700000.gpu: resource[0].start = 0x0x00000000ff700000
[    4.692696] mali-utgard ff700000.gpu: resource[1].start = 0x0x0000000000000015
[    4.692702] mali-utgard ff700000.gpu: resource[2].start = 0x0x0000000000000016
[    4.692707] mali-utgard ff700000.gpu: resource[3].start = 0x0x0000000000000017
[    4.692712] mali-utgard ff700000.gpu: resource[4].start = 0x0x0000000000000018
[    4.692718] mali-utgard ff700000.gpu: resource[5].start = 0x0x0000000000000019
[    4.692723] mali-utgard ff700000.gpu: resource[6].start = 0x0x000000000000001a
[    4.692729] mali-utgard ff700000.gpu: resource[7].start = 0x0x000000000000001b
[    4.692735] D : [File] : drivers/gpu/arm/mali400/mali/platform/rk/rk.c; [Line] : 679; [Func] : mali_platform_device_init(); to add platform_specific_data to platform_device_of_mali.
[    4.693100] mali-utgard ff700000.gpu: leakage=3
[    4.694673] mali-utgard ff700000.gpu: pvtm=842
[    4.694738] mali-utgard ff700000.gpu: pvtm-volt-sel=5
[    4.694849] mmc2: Host Software Queue enabled
[    4.694866] mmc2: new HS400 Enhanced strobe MMC card at address 0001
[    4.695329] mali-utgard ff700000.gpu: avs=0
[    4.695611] mmcblk2: mmc2:0001 95D000 58.7 GiB
[    4.695672] mali-utgard ff700000.gpu: l=-2147483648 h=2147483647 hyst=0 l_limit=0 h_limit=0 h_table=0
[    4.695818] mmcblk2boot0: mmc2:0001 95D000 partition 1 4.00 MiB
[    4.696037] mmcblk2boot1: mmc2:0001 95D000 partition 2 4.00 MiB
[    4.696725] mmcblk2rpmb: mmc2:0001 95D000 partition 3 4.00 MiB, chardev (235:0)
[    4.697468] Mali:
[    4.697475] Mali device driver loaded
[    4.697485] ALSA device list:
[    4.697489] #0: rockchip,hdmi
[    4.697492] #1: rk3528-acodec
[    4.697936] Freeing unused kernel memory: 960K
[    4.700279] mmcblk2: p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14
[    4.715480] Run /init as init process
[    4.715488] with arguments:
[    4.715490] /init
[    4.715492] with environment:
[    4.715494] HOME=/
[    4.715496] TERM=linux
[    4.715499] storagemedia=emmc
[    4.715501] buildvariant=user
[    4.717783] init: init first stage started!
[    4.717944] init: Unable to open /lib/modules, skipping module loading.
[    4.718217] init: Copied ramdisk prop to /second_stage_resources/system/etc/ramdisk/build.prop
[    4.718602] init: [libfs_mgr]ReadFstabFromDt(): failed to read fstab from dt
[    4.720982] init: Using Android DT directory /proc/device-tree/firmware/android/
[    4.721077] init: Failed to read vbmeta partitions.
[    4.744293] vendor storage:20190527 ret = 0
[    4.771605] init: [libfs_mgr]superblock s_max_mnt_count:65535,/dev/block/by-name/metadata
[    4.771758] init: [libfs_mgr]Filesystem on /dev/block/by-name/metadata was not cleanly shutdown; state flags: 0x1, incompat feature flags: 0x46
[    4.772193] EXT4-fs (mmcblk2p11): Ignoring removed nomblk_io_submit option
[    4.777223] EXT4-fs (mmcblk2p11): recovery complete
[    4.777805] EXT4-fs (mmcblk2p11): mounted filesystem with ordered data mode. Opts: errors=remount-ro,nomblk_io_submit
[    4.777961] init: [libfs_mgr]check_fs(): mount(/dev/block/by-name/metadata,/metadata,ext4)=0: Success
[    4.779828] init: [libfs_mgr]umount_retry(): unmount(/metadata) succeeded
[    4.782709] EXT4-fs (mmcblk2p11): mounted filesystem with ordered data mode. Opts: discard
[    4.798535] request_module fs-erofs succeeded, but still no fs?
[    4.802223] EXT4-fs (dm-0): mounted filesystem without journal. Opts: barrier=0
[    4.809709] EXT4-fs (dm-3): mounted filesystem without journal. Opts: barrier=0
[    4.813309] EXT4-fs (dm-5): mounted filesystem without journal. Opts: barrier=0
[    4.816641] EXT4-fs (dm-1): mounted filesystem without journal. Opts: barrier=1
[    4.819862] EXT4-fs (dm-2): mounted filesystem without journal. Opts: barrier=1
[    4.823279] EXT4-fs (dm-4): mounted filesystem without journal. Opts: barrier=1
[    4.826510] EXT4-fs (dm-6): mounted filesystem without journal. Opts: barrier=1
[    4.830213] EXT4-fs (dm-7): mounted filesystem without journal. Opts: barrier=1
[    4.837055] printk: init: 41 output lines suppressed due to ratelimiting
[    4.952184] init: Opening SELinux policy
[    4.954524] init: Falling back to standard signature check. TODO implementent support for fsverity SEPolicy.
[    4.954697] init: Error: Apex SEPolicy failed signature check
[    4.954747] init: Loading APEX Sepolicy from /system/etc/selinux/apex/SEPolicy.zip
[    4.954818] init: Failed to open package /system/etc/selinux/apex/SEPolicy.zip: No such file or directory
[    4.962641] init: Loading SELinux policy
[    4.988292] SELinux:  Permission bpf in class capability2 not defined in policy.
[    4.988301] SELinux:  Permission checkpoint_restore in class capability2 not defined in policy.
[    4.988321] SELinux:  Permission bpf in class cap2_userns not defined in policy.
[    4.988323] SELinux:  Permission checkpoint_restore in class cap2_userns not defined in policy.
[    4.988449] SELinux: the above unknown classes and permissions will be denied
[    4.994284] SELinux:  policy capability network_peer_controls=1
[    4.994293] SELinux:  policy capability open_perms=1
[    4.994296] SELinux:  policy capability extended_socket_class=1
[    4.994298] SELinux:  policy capability always_check_network=0
[    4.994301] SELinux:  policy capability cgroup_seclabel=0
[    4.994304] SELinux:  policy capability nnp_nosuid_transition=1
[    4.994306] SELinux:  policy capability genfs_seclabel_symlinks=0
[    4.994309] SELinux:  policy capability ioctl_skip_cloexec=0
[    5.148812] audit: type=1403 audit(0.926:2): auid=4294967295 ses=4294967295 lsm=selinux res=1
[    5.183528] mmc1: queuing unknown CIS tuple 0x10 (5 bytes)
[    5.189437] mmc_host mmc1: Bus speed (slot 0) = 49800000Hz (slot req 50000000Hz, actual 49800000HZ div = 0)
[    5.189719] mmc1: queuing unknown CIS tuple 0x10 (5 bytes)
[    5.192130] mmc1: new SDIO card at address 80e4
[    5.197411] selinux: SELinux: Loaded file_contexts
[    5.197427] selinux:
[    5.237680] init: init second stage started!
[    5.294891] init: Using Android DT directory /proc/device-tree/firmware/android/
[    5.301157] init: Couldn't load property file '/vendor/default.prop': open() failed: No such file or directory: No such file or directory
[    5.313141] init: Setting product property ro.product.brand to 'Android' (from ro.product.product.brand)
[    5.313185] init: Setting product property ro.product.device to 'rockchip001' (from ro.product.product.device)
[    5.313220] init: Setting product property ro.product.manufacturer to 'RockChip' (from ro.product.product.manufacturer)
[    5.313265] init: Setting product property ro.product.model to 'HK1 RBOX K8' (from ro.product.product.model)
[    5.313297] init: Setting product property ro.product.name to 'rockchip001' (from ro.product.product.name)
[    5.313362] init: Setting property 'ro.product.cpu.abilist' to 'arm64-v8a,armeabi-v7a,armeabi'
[    5.313386] init: Setting property 'ro.product.cpu.abilist32' to 'armeabi-v7a,armeabi'
[    5.353660] audit: type=1400 audit(1.130:3): avc:  denied  { getattr } for  pid=1 comm="init" path="/sbin/su" dev="dm-0" ino=50 scontext=u:r:init:s0 tcontext=u:object_r:su_exec:s0 tclass=file permissive=1
[    5.380893] audit: type=1107 audit(1.156:4): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=u:r:init:s0 msg='avc:  denied  { read } for property=persist.sys.adb_enable pid=0 uid=0 gid=0 scontext=u:r:vendor_init:s0 tcontext=u:object_r:system_prop:s0 tclass=file permissive=1'
[    5.381618] audit: type=1107 audit(1.156:5): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=u:r:init:s0 msg='avc:  denied  { read } for property=flash.success pid=0 uid=0 gid=0 scontext=u:r:vendor_init:s0 tcontext=u:object_r:default_prop:s0 tclass=file permissive=1'
[    5.458617] cgroup: Unknown subsys name 'schedtune'
[    5.481098] audit: type=1400 audit(1.256:6): avc:  denied  { write } for  pid=1 comm="init" name="parameters" dev="sysfs" ino=10181 scontext=u:r:init:s0 tcontext=u:object_r:sysfs:s0 tclass=dir permissive=1
[    5.481111] audit: type=1400 audit(1.256:7): avc:  denied  { add_name } for  pid=1 comm="init" name="prefetch_cluster" scontext=u:r:init:s0 tcontext=u:object_r:sysfs:s0 tclass=dir permissive=1
[    5.481116] audit: type=1400 audit(1.256:8): avc:  denied  { create } for  pid=1 comm="init" name="prefetch_cluster" scontext=u:r:init:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[    5.500453] linkerconfig: Unable to access VNDK APEX at path: /apex/com.android.vndk.v33: No such file or directory
[    5.500604] linkerconfig: Unable to access VNDK APEX at path: /apex/com.android.vndk.v33: No such file or directory
[    5.541675] ueventd: ueventd started!
[    5.546583] selinux: SELinux: Loaded file_contexts
[    5.546598] selinux:
[    5.547276] ueventd: Parsing file /system/etc/ueventd.rc...
[    5.547959] ueventd: Added '/vendor/etc/ueventd.rc' to import list
[    5.548052] ueventd: Added '/odm/etc/ueventd.rc' to import list
[    5.548677] ueventd: Parsing file /vendor/etc/ueventd.rc...
[    5.550650] ueventd: Parsing file /odm/etc/ueventd.rc...
[    5.550762] ueventd: Unable to read config file '/odm/etc/ueventd.rc': open() failed: No such file or directory
[    5.578540] apexd: Bootstrap subcommand detected
[    5.578615] apexd: ActivateFlattenedApex
[    5.578650] apexd: Scanning /system/apex
[    5.580723] apexd: Bind mounting /system/apex/com.android.adbd onto /apex/com.android.adbd
[    5.582044] apexd: Bind mounting /system/apex/com.android.adservices onto /apex/com.android.adservices
[    5.583377] apexd: Bind mounting /system/apex/com.android.appsearch onto /apex/com.android.appsearch
[    5.584798] apexd: Bind mounting /system/apex/com.android.art onto /apex/com.android.art
[    5.586241] apexd: Bind mounting /system/apex/com.android.btservices onto /apex/com.android.btservices
[    5.587594] apexd: Bind mounting /system/apex/com.android.conscrypt onto /apex/com.android.conscrypt
[    5.590172] apexd: Bind mounting /system/apex/com.android.extservices onto /apex/com.android.extservices
[    5.630649] printk: apexd: 24 output lines suppressed due to ratelimiting
[    6.047082] audit: type=1400 audit(1.823:9): avc:  denied  { mounton } for  pid=1 comm="init" path="/sys/kernel/debug/tracing" dev="debugfs" ino=1180 scontext=u:r:init:s0 tcontext=u:object_r:debugfs_tracing_debug:s0 tclass=dir permissive=1
[    6.058103] audit: type=1400 audit(1.833:10): avc:  denied  { getattr } for  pid=131 comm="init" path="/sys/kernel/debug/tracing/trace_marker" dev="tracefs" ino=10358 scontext=u:r:vendor_init:s0 tcontext=u:object_r:debugfs_trace_marker:s0 tclass=file permissive=1
[    6.122871] aic8800_bsp: loading out-of-tree module taints kernel.
[    6.124285] aicbsp_init
[    6.124293] RELEASE_DATE:2023_0207_1037\x0d
[    6.124809] aicbsp: aicbsp_set_subsys, subsys: AIC_BLUETOOTH, state to: 1
[    6.124820] aicbsp: aicbsp_set_subsys, power state change to 1 dure to AIC_BLUETOOTH
[    6.124822] aicbsp: aicbsp_platform_power_on
[    6.140574] aicbsp: aicbsp_sdio_probe:1 vid:0x5449  did:0x0145
[    6.140690] aicbsp: aicbsp_sdio_probe:2 vid:0x544A  did:0x0146
[    6.140696] aicbsp: aicbsp_sdio_probe after replace:1
[    6.140700] AICWFDBG(LOGINFO)\x09aicwf_sdio_chipmatch USE AIC8801\x0d
[    6.140705] aicbsp: aicbsp_get_feature, set FEATURE_SDIO_CLOCK 50 MHz
[    6.140707] aicbsp: aicwf_sdio_reg_init
[    6.140899] aicbsp: Set SDIO Clock 50 MHz
[    6.146894] AICWFDBG(LOGINFO)\x09aicbsp: aicbsp_driver_fw_init, chip rev: 7
[    6.146908] rwnx_load_firmware :firmware path = /vendor/etc/firmware/fw_patch_table_u03.bin
[    6.148224] file md5:30ab35aef2699b788f7b0618e5363b74\x0d
[    6.148297] rwnx_plat_bin_fw_upload_android
[    6.148302] rwnx_load_firmware :firmware path = /vendor/etc/firmware/fw_adid_u03.bin
[    6.148946] file md5:cf3ee68167beda73aaa5cb7a17169b4d\x0d
[    6.149526] rwnx_plat_bin_fw_upload_android
[    6.149539] rwnx_load_firmware :firmware path = /vendor/etc/firmware/fw_patch_u03.bin
[    6.151136] file md5:4693413e427b7d31ce733d74f9cc0c7e\x0d
[    6.187030] aicbt_patch_table_load bt btmode:0 \x0d
[    6.187041] aicbt_patch_table_load bt uart_baud:1500000 \x0d
[    6.187044] aicbt_patch_table_load bt uart_flowctrl:1 \x0d
[    6.187046] aicbt_patch_table_load bt lpm_enable:0 \x0d
[    6.187049] aicbt_patch_table_load bt tx_pwr:24608 \x0d
[    6.196677] aicbsp: bt patch version: - Feb 22 2023 11:07:28 - git 479f66a
[    6.196739] rwnx_plat_bin_fw_upload_android
[    6.196745] rwnx_load_firmware :firmware path = /vendor/etc/firmware/fmacfw.bin
[    6.200655] file md5:cc35035cd7eba5369f1bba20c3b26e17\x0d
[    6.362516] ueventd: Coldboot took 0.81 seconds
[    6.493300] Registered swp emulation handler
[    6.633445] EXT4-fs (mmcblk2p10): Ignoring removed nomblk_io_submit option
[    6.651091] EXT4-fs (mmcblk2p10): recovery complete
[    6.652189] EXT4-fs (mmcblk2p10): mounted filesystem with ordered data mode. Opts: errors=remount-ro,nomblk_io_submit
[    6.658094] logd.auditd: start
[    6.658122] logd.klogd: 2436066984
[    6.670022] logd: Loaded bug_map file: /system_ext/etc/selinux/bug_map
[    6.670305] logd: Loaded bug_map file: /vendor/etc/selinux/selinux_denial_metadata
[    6.671629] logd: Loaded bug_map file: /system/etc/selinux/bug_map
[    6.696841] e2fsck: e2fsck 1.46.2 (28-Feb-2021)
[    6.703011] e2fsck: Pass 1: Checking inodes, blocks, and sizes
[    6.706385] e2fsck: Pass 2: Checking directory structure
[    6.708764] e2fsck: Pass 3: Checking directory connectivity
[    6.709413] e2fsck: Pass 4: Checking reference counts
[    6.715741] e2fsck: Pass 5: Checking group summary information
[    6.729426] e2fsck: /dev/block/by-name/cache: 18/90112 files (0.0% non-contiguous), 19513/360448 blocks
[    6.736069] EXT4-fs (mmcblk2p10): mounted filesystem with ordered data mode. Opts: noauto_da_alloc,discard
[    6.809891] PELT half life is set to 8ms
[    6.940488] EXT4-fs (mmcblk2p14): Ignoring removed nomblk_io_submit option
[    6.940509] EXT4-fs (mmcblk2p14): Using encoding defined by superblock: utf8-12.1.0 with flags 0x0
[    7.024148] EXT4-fs (mmcblk2p14): recovery complete
[    7.025586] EXT4-fs (mmcblk2p14): mounted filesystem with ordered data mode. Opts: errors=remount-ro,nomblk_io_submit
[    7.073016] e2fsck: e2fsck 1.46.2 (28-Feb-2021)
[    7.184394] e2fsck: Pass 1: Checking inodes, blocks, and sizes
[    7.309072] type=1107 audit(3.083:17): uid=0 auid=4294967295 ses=4294967295 subj=u:r:init:s0 msg='avc: denied { set } for property=vendor.gmali.version pid=185 uid=1000 gid=1003 scontext=u:r:surfaceflinger:s0 tcontext=u:object_r:vendor_system_public_prop:s0 tclass=property_service permissive=1'
[    7.316132] type=1400 audit(3.093:18): avc: denied { search } for comm="surfaceflinger" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:surfaceflinger:s0 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1
[    7.487343] e2fsck: Pass 2: Checking directory structure
[    8.079468] EXT4-fs (mmcblk2p14): Ignoring removed nomblk_io_submit option
[    8.079487] EXT4-fs (mmcblk2p14): Using encoding defined by superblock: utf8-12.1.0 with flags 0x0
[    8.086818] EXT4-fs (mmcblk2p14): mounted filesystem with ordered data mode. Opts: discard,nodelalloc,nomblk_io_submit,errors=continue
[    8.101339] type=1400 audit(3.876:19): avc: denied { add_name } for comm="init" name="iostats" scontext=u:r:vendor_init:s0 tcontext=u:object_r:sysfs:s0 tclass=dir permissive=1
[    8.101596] type=1400 audit(3.876:20): avc: denied { create } for comm="init" name="iostats" scontext=u:r:vendor_init:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[    8.130848] type=1400 audit(3.906:21): avc: denied { write } for comm="init" name="queue" dev="sysfs" ino=25606 scontext=u:r:vendor_init:s0 tcontext=u:object_r:sysfs_dm:s0 tclass=dir permissive=1
[    8.270299] vold: keystore2 Keystore earlyBootEnded returned service specific error: -68
[    8.327388] apexd: This device does not support updatable APEX. Exiting
[    8.327535] apexd: Marking APEXd as activated
[    8.501724] rockchip-vop2 ff840000.vop: [drm:vop2_crtc_atomic_enable] Update mode to 1920x1080p60, type: 11(if:800) for vp0 dclk: 148500000
[    8.589798] vdc: Waited 0ms for vold
[    8.643316] [drm:vop2_plane_atomic_check] *ERROR* Esmart3-win0 is invisible(src: pos[0, 0] rect[1280 x 720] dst: pos[0, 0] rect[720 x 576]
[    8.643360] rockchip-vop2 ff840000.vop: [drm:vop2_crtc_atomic_disable] Crtc atomic disable vp1
[    8.834599] apexd: This device does not support updatable APEX. Exiting
[    8.834682] apexd: Marking APEXd as ready
[    8.963701] LibBpfLoader: Loading optional ELF object /apex/com.android.tethering/etc/bpf/test.o with license Apache 2.0
[    8.963950] LibBpfLoader: Section bpfloader_min_ver value is 2 [0x2]
[    8.964150] LibBpfLoader: Section bpfloader_max_ver value is 65536 [0x10000]
[    8.964270] LibBpfLoader: Section size_of_bpf_map_def value is 116 [0x74]
[    8.964404] LibBpfLoader: Section size_of_bpf_prog_def value is 92 [0x5c]
[    8.964428] LibBpfLoader: BpfLoader version 0x00013 processing ELF object /apex/com.android.tethering/etc/bpf/test.o with ver [0x00002,0x10000)
[    8.965250] LibBpfLoader: Loaded code section 3 (xdp_drop_ipv4_udp_ether)
[    8.965802] LibBpfLoader: Adding section 3 to cs list
[    8.966994] LibBpfLoader: Couldn't find section btf_min_bpfloader_ver (defaulting to 0 [0x0]).
[    8.967137] LibBpfLoader: Couldn't find section btf_min_kernel_ver (defaulting to 0 [0x0]).
[    9.488257] printk: bpfloader: 1683 output lines suppressed due to ratelimiting
[    9.576719] init: Inferred action different from explicit one, expected 0 but got 2
[    9.613027] file system registered
[    9.618469] using random self ethernet address
[    9.618482] using random host ethernet address
[    9.747176] using random self ethernet address
[    9.747188] using random host ethernet address
[   10.221942] healthd: BatteryTemperaturePath not found
[   10.221968] healthd: BatteryCycleCountPath not found
[   10.221974] healthd: batteryCapacityLevelPath not found
[   10.221981] healthd: batteryChargeTimeToFullNowPath. not found
[   10.221986] healthd: batteryFullChargeDesignCapacityUahPath. not found
[   10.270628] init: starting service 'vendor.light-rockchip'...
[   10.313324] init: starting service 'vendor.power-aidl-rockchip'...
[   10.368017] init: starting service 'vendor.outputmanager-1-0'...
[   10.418144] init: starting service 'vendor.rockit-hal-1-0'...
[   10.427145] init: Command 'class_start hal' action=boot (/system/etc/init/hw/init.rc:1180) took 557ms and succeeded
[   10.427580] init: service 'ueventd' requested start, but it is already running (flags: 2084)
[   10.427666] init: service 'console' requested start, but it is already running (flags: 20)
[   10.428016] init: Could not start service 'displayd' as part of class 'core': Cannot find '/system/bin/displayd': No such file or directory
[   10.429182] init: starting service 'audioserver'...
[   10.510437] init: service 'bootanim' requested start, but it is already running (flags: 134)
[   10.663195] aicbsp: aicbsp_set_subsys, subsys: AIC_WIFI, state to: 1
[   10.663206] aicbsp: aicbsp_set_subsys, power state no need to change, current: 1
[   10.663389] aicbsp: aicbsp_get_feature, set FEATURE_SDIO_CLOCK 50 MHz
[   10.663398] aicsdio: aicwf_sdio_reg_init
[   10.675930] aicbsp: aicbsp_get_feature, set FEATURE_SDIO_CLOCK 50 MHz
[   10.677832] userconfig file path:aic_userconfig.txt \x0d
[   10.686396] ### Upload aic_userconfig.txt userconfig, size=1193
[   10.686410] rwnx_plat_userconfig_parsing rwnx_hw->vendor_info:0x00 \x0d
[   10.686412] Unsuppor vendor info config
[   10.686414] Using module0 config
[   10.686444] rwnx_plat_userconfig_parsing, enable = 1
[   10.686448] rwnx_plat_userconfig_parsing, dsss = 9
[   10.686452] rwnx_plat_userconfig_parsing, ofdmlowrate_2g4 = 10
[   10.686456] rwnx_plat_userconfig_parsing, ofdm64qam_2g4 = 10
[   10.686461] rwnx_plat_userconfig_parsing, ofdm256qam_2g4 = 10
[   10.686466] rwnx_plat_userconfig_parsing, ofdm1024qam_2g4 = 8
[   10.686472] rwnx_plat_userconfig_parsing, ofdmlowrate_5g = 11
[   10.686478] rwnx_plat_userconfig_parsing, ofdm64qam_5g = 10
[   10.686485] rwnx_plat_userconfig_parsing, ofdm256qam_5g = 10
[   10.686491] rwnx_plat_userconfig_parsing, ofdm1024qam_5g = 9
[   10.686506] rwnx_plat_userconfig_parsing, ofst_enable = 0
[   10.686513] rwnx_plat_userconfig_parsing, ofst_chan_1_4 = 0
[   10.686521] rwnx_plat_userconfig_parsing, ofst_chan_5_9 = 0
[   10.686529] rwnx_plat_userconfig_parsing, ofst_chan_10_13 = 0
[   10.686538] rwnx_plat_userconfig_parsing, ofst_chan_36_64 = 0
[   10.686547] rwnx_plat_userconfig_parsing, ofst_chan_100_120 = 0
[   10.686557] rwnx_plat_userconfig_parsing, ofst_chan_122_140 = 0
[   10.686567] rwnx_plat_userconfig_parsing, ofst_chan_142_165 = 0
[   10.686584] rwnx_plat_userconfig_parsing, xtal_enable = 0
[   10.686594] rwnx_plat_userconfig_parsing, xtal_cap = 24
[   10.686606] rwnx_plat_userconfig_parsing, xtal_cap_fine = 31
[   10.686820] userconfig download complete\x0a
[   11.088072] ieee80211 phy0: HT supp 1, VHT supp 1, HE supp 1
[   11.090323] AICWFDBG(LOGERROR)\x09rwnx_get_countrycode_channels support channel:1 2 3 4 5 6 7 8 9 10 11 12 13 14 36 40 44 48 52 56 60 64 100 104 108 112 116 120 124 128 132 136 140 144 149 153 157 161 165 \x0d
[   11.090386] ieee80211 phy0: \x0a*******************************************************\x0a** CAUTION: USING PERMISSIVE CUSTOM REGULATORY RULES **\x0a*******************************************************
[   11.424855] FD628: fd628_dev_open \x0d
[   11.615211] rk_gmac-dwmac ffbd0000.ethernet eth0: PHY [stmmac-0:02] driver [RK630 PHY] (irq=POLL)
[   11.625618] alloc_contig_range: [fb902, fb904) PFNs busy
[   11.635701] alloc_contig_range: [fb904, fb906) PFNs busy
[   11.645996] alloc_contig_range: [fb906, fb908) PFNs busy
[   11.671246] dwmac4: Master AXI performs any burst length
[   11.671277] rk_gmac-dwmac ffbd0000.ethernet eth0: No Safety Features support found
[   11.671290] rk_gmac-dwmac ffbd0000.ethernet eth0: PTP not supported by HW
[   11.683990] rk_gmac-dwmac ffbd0000.ethernet eth0: configuring for phy/rmii link mode
[   11.879807] type=1400 audit(7.656:67): avc: denied { search } for comm="rockchip.drmser" name="block" dev="tmpfs" ino=12 scontext=u:r:np_rockchip_drmservice:s0 tcontext=u:object_r:block_device:s0 tclass=dir permissive=1
[   11.882740] type=1400 audit(7.656:68): avc: denied { read } for comm="rockchip.drmser" name="mmcblk2boot0" dev="tmpfs" ino=421 scontext=u:r:np_rockchip_drmservice:s0 tcontext=u:object_r:uboot_block_device:s0 tclass=blk_file permissive=1
[   11.883079] type=1400 audit(7.656:69): avc: denied { open } for comm="rockchip.drmser" path="/dev/block/mmcblk2boot0" dev="tmpfs" ino=421 scontext=u:r:np_rockchip_drmservice:s0 tcontext=u:object_r:uboot_block_device:s0 tclass=blk_file permissive=1
[   11.883379] type=1400 audit(7.656:70): avc: denied { write } for comm="rockchip.drmser" name="mmcblk2" dev="tmpfs" ino=384 scontext=u:r:np_rockchip_drmservice:s0 tcontext=u:object_r:uboot_block_device:s0 tclass=blk_file permissive=1
[   11.887522] type=1400 audit(7.656:71): avc: denied { getattr } for comm="rockchip.drmser" path="/dev/block/mmcblk2" dev="tmpfs" ino=384 scontext=u:r:np_rockchip_drmservice:s0 tcontext=u:object_r:uboot_block_device:s0 tclass=blk_file permissive=1
[   17.186234] logd: logdr: UID=1041 GID=1005 PID=473 n tail=0 logMask=8 pid=279 start=0ns deadline=0ns
[   17.187305] logd: logdr: UID=1041 GID=1005 PID=473 n tail=0 logMask=1 pid=279 start=0ns deadline=0ns
[   17.250180] init: Untracked pid 473 exited with status 0
[   17.250222] init: Untracked pid 473 did not have an associated service entry and will not be reaped
[   17.251971] init: Untracked pid 475 exited with status 0
[   17.252002] init: Untracked pid 475 did not have an associated service entry and will not be reaped
[   17.884852] type=1400 audit(13.660:72): avc: denied { read } for comm="binder:179_5" name="wakeup4" dev="sysfs" ino=15010 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=dir permissive=1
[   17.885251] type=1400 audit(13.660:73): avc: denied { open } for comm="binder:179_5" path="/sys/devices/platform/ff740100.rkvdec/wakeup/wakeup4" dev="sysfs" ino=15010 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=dir permissive=1
[   17.885628] type=1400 audit(13.660:74): avc: denied { read } for comm="binder:179_5" name="event_count" dev="sysfs" ino=15017 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[   17.885863] type=1400 audit(13.660:75): avc: denied { open } for comm="binder:179_5" path="/sys/devices/platform/ff740100.rkvdec/wakeup/wakeup4/event_count" dev="sysfs" ino=15017 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[   17.886049] type=1400 audit(13.660:76): avc: denied { getattr } for comm="binder:179_5" path="/sys/devices/platform/ff740100.rkvdec/wakeup/wakeup4/event_count" dev="sysfs" ino=15017 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[   18.126865] rockchip-vop2 ff840000.vop: [drm:vop2_crtc_atomic_enable] Update mode to 720x576i50, type: 13(if:4) for vp1 dclk: 27000000
[   18.251547] logd: logdr: UID=1041 GID=1005 PID=507 n tail=0 logMask=8 pid=320 start=0ns deadline=0ns
[   18.252711] logd: logdr: UID=1041 GID=1005 PID=507 n tail=0 logMask=1 pid=320 start=0ns deadline=0ns
[   18.302310] init: Untracked pid 507 exited with status 0
[   18.302343] init: Untracked pid 507 did not have an associated service entry and will not be reaped
[   18.302532] init: Untracked pid 510 exited with status 0
[   18.302552] init: Untracked pid 510 did not have an associated service entry and will not be reaped
[   18.302916] binder: release 320:415 transaction 885 in, still active
[   18.302928] binder: send failed reply for transaction 885 to 186:261
[   18.303776] init: Service 'audioserver' (pid 320) received signal 6
[   18.303823] init: Sending signal 9 to service 'audioserver' (pid 320) process group...
[   19.195805] rockchip-vop2 ff840000.vop: [drm:vop2_crtc_atomic_disable] Crtc atomic disable vp0
[   19.303013] rockchip-vop2 ff840000.vop: [drm:vop2_crtc_atomic_disable] Crtc atomic disable vp1
[   19.330677] rockchip-vop2 ff840000.vop: [drm:vop2_crtc_atomic_enable] Update mode to 1920x1080p60, type: 11(if:800) for vp0 dclk: 148500000
[   20.354185] Freeing drm_logo memory: 10808K
[   20.930266] type=1400 audit(16.706:77): avc: denied { call } for comm="BootAnimation" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   23.103963] type=1400 audit(1694786145.626:97): avc: denied { call } for comm="binder:186_2" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   23.624844] type=1400 audit(1694786146.146:98): avc: denied { read write } for comm="cec@1.0-service" name="rk_cec" dev="tmpfs" ino=473 scontext=u:r:hal_tv_cec_default:s0 tcontext=u:object_r:device:s0 tclass=chr_file permissive=1
[   23.625148] type=1400 audit(1694786146.146:99): avc: denied { open } for comm="cec@1.0-service" path="/dev/rk_cec" dev="tmpfs" ino=473 scontext=u:r:hal_tv_cec_default:s0 tcontext=u:object_r:device:s0 tclass=chr_file permissive=1
[   23.625506] type=1400 audit(1694786146.146:100): avc: denied { ioctl } for comm="cec@1.0-service" path="/dev/rk_cec" dev="tmpfs" ino=473 ioctlcmd=0x610b scontext=u:r:hal_tv_cec_default:s0 tcontext=u:object_r:device:s0 tclass=chr_file permissive=1
[   24.071833] init: processing action (wlan.driver.status=ok) from (/vendor/etc/init/hw/init.connectivity.rc:52)
[   24.102913] init: processing action (vendor.wifi.direct.interface=p2p-dev-wlan0) from (/vendor/etc/init/hw/init.connectivity.rc:59)
[   24.140403] init: processing action (vendor.wifi.direct.interface=p2p-dev-wlan0) from (/vendor/etc/init/hw/init.connectivity.rc:59)
[   24.154959] init: processing action (vendor.wifi.direct.interface=p2p-dev-wlan0) from (/vendor/etc/init/hw/init.connectivity.rc:59)
[   24.159653] init: processing action (wlan.driver.status=ok) from (/vendor/etc/init/hw/init.connectivity.rc:52)
[   24.163512] wlan0 selects TX queue 256, but real number of TX queues is 81
[   24.172207] wlan0 selects TX queue 256, but real number of TX queues is 81
[   24.229974] init: processing action (vendor.wifi.direct.interface=p2p-dev-wlan0) from (/vendor/etc/init/hw/init.connectivity.rc:59)
[   24.235540] wlan0 selects TX queue 256, but real number of TX queues is 81
[   24.263517] init: starting service 'wpa_supplicant'...
[   24.265268] init: Created socket '/dev/socket/wpa_wlan0', mode 660, user 1010, group 1010
[   24.274754] init: Control message: Processed ctl.interface_start for 'aidl/android.hardware.wifi.supplicant.ISupplicant/default' from pid: 153 (/system/bin/servicemanager)
[   24.411653] capability: warning: `wpa_supplicant' uses 32-bit capabilities (legacy support in use)
[   24.526398] rwnx_virtual_interface_add: 10, p2p-dev-wlan0
[   24.526414] rwnx_virtual_interface_add, ifname=p2p-dev-wlan0, wdev=000000008571f4b3, vif_idx=1
[   24.526420] p2p dev addr=68 8f c9 0 40 cb
[   24.545898] P2P interface started
[   24.667170] AICWFDBG(LOGERROR)\x09rwnx_get_countrycode_channels support channel:1 2 3 4 5 6 7 8 9 10 11 12 13 36 40 44 48 52 56 60 64 100 104 108 112 116 120 124 128 132 136 140 144 149 153 157 161 165 \x0d
[   24.667238] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   25.092495] type=1400 audit(1694786147.610:101): avc: denied { call } for comm="SyncFence" scontext=u:r:mediaserver:s0 tcontext=u:r:bootanim:s0 tclass=binder permissive=1
[   25.117159] type=1400 audit(1694786147.636:102): avc: denied { call } for comm="binder:186_2" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   25.182259] wlan0 selects TX queue 256, but real number of TX queues is 81
[   25.182387] wlan0 selects TX queue 256, but real number of TX queues is 81
[   25.375524] wlan0 selects TX queue 256, but real number of TX queues is 81
[   26.353276] type=1400 audit(1694786148.710:103): avc: denied { call } for comm="binder:186_2" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   26.353660] type=1400 audit(1694786148.786:104): avc: denied { call } for comm="dequeBufferProc" scontext=u:r:mediaserver:s0 tcontext=u:r:bootanim:s0 tclass=binder permissive=1
[   26.482290] type=1400 audit(1694786148.893:105): avc: denied { use } for comm="bootanimation" path="anon_inode:sync_file" dev="anon_inodefs" ino=10428 scontext=u:r:mediaserver:s0 tcontext=u:r:bootanim:s0 tclass=fd permissive=1
[   26.994635] type=1400 audit(1694786149.026:106): avc: denied { call } for comm="SyncFence" scontext=u:r:mediaserver:s0 tcontext=u:r:bootanim:s0 tclass=binder permissive=1
[   27.389803] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   27.390878] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   27.393541] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   27.394480] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   27.395529] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   27.398041] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   27.399493] AICWFDBG(LOGERROR)\x09wrong cmd:(null) in handle_private_cmd
[   27.933708] dw-apb-uart ffa00000.serial: failed to request DMA, use interrupt mode
[   28.853294] init: processing action (sys.usb.config=none && sys.usb.configfs=1) from (/vendor/etc/init/hw/init.rk30board.usb.rc:123)
[   28.931936] type=1400 audit(1694786151.360:116): avc: denied { search } for comm="RenderThread" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:platform_app:s0:c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.android.systemui
[   28.932301] type=1400 audit(1694786151.393:117): avc: denied { call } for comm="SyncFence" scontext=u:r:mediaserver:s0 tcontext=u:r:bootanim:s0 tclass=binder permissive=1
[   28.962436] logd: start watching /data/system/packages.list ...
[   28.964970] logd: ReadPackageList, total packages: 80
[   29.080651] type=1400 audit(1694786151.460:118): avc: denied { call } for comm="binder:186_2" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   29.081333] type=1400 audit(1694786151.526:119): avc: denied { call } for comm="SyncFence" scontext=u:r:mediaserver:s0 tcontext=u:r:bootanim:s0 tclass=binder permissive=1
[   29.081666] type=1400 audit(1694786151.543:120): avc: denied { call } for comm="binder:186_2" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   29.098385] init: starting service 'adbd'...
[   29.115699] init: Created socket '/dev/socket/adbd', mode 660, user 1000, group 1000
[   29.143566] type=1400 audit(1694786151.553:121): avc: denied { write } for comm="init" name="interactive" dev="sysfs" ino=28984 scontext=u:r:vendor_init:s0 tcontext=u:object_r:sysfs_devices_system_cpu:s0 tclass=dir permissive=1
[   29.143914] type=1400 audit(1694786151.553:122): avc: denied { add_name } for comm="init" name="touchboost_freq" scontext=u:r:vendor_init:s0 tcontext=u:object_r:sysfs_devices_system_cpu:s0 tclass=dir permissive=1
[   29.144228] type=1400 audit(1694786151.553:123): avc: denied { create } for comm="init" name="touchboost_freq" scontext=u:r:vendor_init:s0 tcontext=u:object_r:sysfs_devices_system_cpu:s0 tclass=file permissive=1
[   29.163715] init: Command 'restart adbd' action=sys.boot_completed=1 (/vendor/etc/init/hw/init.rk3528.rc:61) took 67ms and succeeded
[   29.164137] init: processing action (sys.boot_completed=1 && sys.bootstat.first_boot_completed=0) from (/system/etc/init/bootstat.rc:77)
[   29.165188] init: starting service 'exec 17 (/system/bin/bootstat --record_boot_complete --record_boot_reason --record_time_since_factory_reset -l)'...
[   29.190467] init: Service 'preinstall' (pid 1103) exited with status 0 oneshot service took 0.146000 seconds in background
[   29.190562] init: Sending signal 9 to service 'preinstall' (pid 1103) process group...
[   29.191037] libprocessgroup: Successfully killed process cgroup uid 0 pid 1103 in 0ms
[   29.201410] init: processing action (sys.boot_completed=1) from (/system/etc/init/flags_health_check.rc:7)
[   29.223328] init: processing action (sys.boot_completed=1) from (/system/etc/init/logd.rc:34)
[   29.593862] zram0: detected capacity change from 0 to 1231032320
[   29.627268] mkswap: Swapspace size: 1202176k, UUID=577bda24-7cf3-4bf7-b9ec-2195a3c95182
[   29.648916] Adding 1202176k swap on /dev/block/zram0.  Priority:-2 extents:1 across:1202176k SS
[   31.443198] audit_log_lost: 6 callbacks suppressed
[   31.443206] audit: audit_lost=1 audit_rate_limit=5 audit_backlog_limit=64
[   31.443214] audit: rate limit exceeded
[   31.445544] dw-apb-uart ffa08000.serial: failed to request DMA, use interrupt mode
[   34.132961] type=1400 audit(1694786156.656:155): avc: granted { execute } for comm="Thread-2" path="/data/user/0/com.waxrain.airplaydmr3/files/libnview.so" dev="mmcblk2p14" ino=1663967 scontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tcontext=u:object_r:app_data_file:s0:c78,c256,c512,c768 tclass=file app=com.waxrain.airplaydmr3
[   34.174162] audit: audit_lost=8 audit_rate_limit=5 audit_backlog_limit=64
[   34.174174] audit: rate limit exceeded
[   34.176215] type=1400 audit(1694786156.696:156): avc: denied { open } for comm=".update.service" path="/dev/__properties__/u:object_r:serialno_prop:s0" dev="tmpfs" ino=257 scontext=u:r:system_app:s0 tcontext=u:object_r:serialno_prop:s0 tclass=file permissive=1
[   34.178070] type=1400 audit(1694786156.700:159): avc: denied { call } for comm="BootAnimation" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   34.329767] type=1400 audit(1694786156.843:160): avc: denied { search } for comm="RenderThread" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.waxrain.airplaydmr3
[   34.685798] type=1400 audit(1694786157.203:161): avc: denied { call } for comm="BootAnimation" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   34.739311] type=1400 audit(1694786157.203:162): avc: denied { write } for comm="android.bootfix" name="time" dev="sysfs" ino=10267 scontext=u:r:system_app:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[   34.761098] type=1400 audit(1694786157.203:163): avc: denied { open } for comm="android.bootfix" path="/sys/module/aml_fd628/parameters/time" dev="sysfs" ino=10267 scontext=u:r:system_app:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[   35.186448] type=1400 audit(1694786157.710:165): avc: denied { call } for comm="BootAnimation" scontext=u:r:bootanim:s0 tcontext=u:r:mediaserver:s0 tclass=binder permissive=1
[   35.260374] type=1400 audit(1694786157.783:166): avc: denied { read } for comm="pool-3-thread-1" name="version" dev="proc" ino=4026532103 scontext=u:r:untrusted_app:s0:c57,c256,c512,c768 tcontext=u:object_r:proc_version:s0 tclass=file permissive=1 app=org.courville.nova
[   35.260918] type=1400 audit(1694786157.783:167): avc: denied { open } for comm="pool-3-thread-1" path="/proc/version" dev="proc" ino=4026532103 scontext=u:r:untrusted_app:s0:c57,c256,c512,c768 tcontext=u:object_r:proc_version:s0 tclass=file permissive=1 app=org.courville.nova
[   35.804482] binder: undelivered transaction 42482, process died.
[   35.806029] init: Service 'bootanim' (pid 186) exited with status 0 oneshot service took 28.875999 seconds in background
[   35.806072] init: Sending signal 9 to service 'bootanim' (pid 186) process group...
[   35.806516] libprocessgroup: Successfully killed process cgroup uid 1003 pid 186 in 0ms
[   39.238879] init: Sending signal 9 to service 'idmap2d' (pid 379) process group...
[   39.245022] libprocessgroup: Successfully killed process cgroup uid 1000 pid 379 in 6ms
[   39.246151] init: Control message: Processed ctl.stop for 'idmap2d' from pid: 449 (system_server)
[   39.246671] init: Service 'idmap2d' (pid 379) received signal 9
[   41.752825] type=1400 audit(1694786164.270:170): avc: denied { execute } for comm="Thread-2" name="su" dev="dm-0" ino=4286 scontext=u:r:system_app:s0 tcontext=u:object_r:su_exec:s0 tclass=file permissive=1
[   41.753833] type=1400 audit(1694786164.270:171): avc: denied { read open } for comm="Thread-2" path="/system/xbin/su" dev="dm-0" ino=4286 scontext=u:r:system_app:s0 tcontext=u:object_r:su_exec:s0 tclass=file permissive=1
[   41.754150] type=1400 audit(1694786164.270:172): avc: denied { execute_no_trans } for comm="Thread-2" path="/system/xbin/su" dev="dm-0" ino=4286 scontext=u:r:system_app:s0 tcontext=u:object_r:su_exec:s0 tclass=file permissive=1
[   41.754476] type=1400 audit(1694786164.270:173): avc: denied { map } for comm="su" path="/system/xbin/su" dev="dm-0" ino=4286 scontext=u:r:system_app:s0 tcontext=u:object_r:su_exec:s0 tclass=file permissive=1
[   41.755088] type=1400 audit(1694786164.276:174): avc: denied { getattr } for comm="su" path="/system/xbin/su" dev="dm-0" ino=4286 scontext=u:r:system_app:s0 tcontext=u:object_r:su_exec:s0 tclass=file permissive=1
[   42.407675] init: Untracked pid 1993 exited with status 0
[   42.407719] init: Untracked pid 1993 did not have an associated service entry and will not be reaped
[   46.907366] type=1400 audit(1694786169.430:175): avc: denied { bind } for comm="DnsConfigServic" scontext=u:r:untrusted_app_27:s0:c512,c768 tcontext=u:r:untrusted_app_27:s0:c512,c768 tclass=netlink_route_socket permissive=1 app=com.google.android.youtube.tv
[   46.907998] type=1400 audit(1694786169.430:176): avc: denied { nlmsg_readpriv } for comm="DnsConfigServic" scontext=u:r:untrusted_app_27:s0:c512,c768 tcontext=u:r:untrusted_app_27:s0:c512,c768 tclass=netlink_route_socket permissive=1 app=com.google.android.youtube.tv
[   48.335194] type=1400 audit(1694786170.856:177): avc: denied { search } for comm="binder:2180_2" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:gmscore_app:s0:c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.google.android.gms
[   49.254394] type=1400 audit(1694786171.776:178): avc: denied { search } for comm="ValueStore-1" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:priv_app:s0:c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.android.vending
[   50.261784] type=1400 audit(1694786172.783:179): avc: denied { open } for comm="oid.tv.settings" path="/dev/__properties__/u:object_r:vendor_default_prop:s0" dev="tmpfs" ino=303 scontext=u:r:system_app:s0 tcontext=u:object_r:vendor_default_prop:s0 tclass=file permissive=1
[   50.262632] type=1400 audit(1694786172.783:180): avc: denied { getattr } for comm="oid.tv.settings" path="/dev/__properties__/u:object_r:vendor_default_prop:s0" dev="tmpfs" ino=303 scontext=u:r:system_app:s0 tcontext=u:object_r:vendor_default_prop:s0 tclass=file permissive=1
[   50.262999] type=1400 audit(1694786172.783:181): avc: denied { map } for comm="oid.tv.settings" path="/dev/__properties__/u:object_r:vendor_default_prop:s0" dev="tmpfs" ino=303 scontext=u:r:system_app:s0 tcontext=u:object_r:vendor_default_prop:s0 tclass=file permissive=1
[   56.172425] binder: undelivered TRANSACTION_COMPLETE
[   56.172448] binder: undelivered transaction 62663, process died.
[   56.173590] binder: undelivered transaction 62700, process died.
[   58.833061] binder: undelivered TRANSACTION_COMPLETE
[   58.833084] binder: undelivered transaction 67062, process died.
[   59.146072] binder: 1070:1562 transaction failed 29189/-22, size 168-0 line 3147
[   59.167462] binder: 1070:1562 transaction failed 29189/-22, size 376-0 line 3147
[   59.203872] binder: 1070:1375 transaction failed 29189/-22, size 168-0 line 3147
[   61.115609] type=1400 audit(1694786183.630:182): avc: denied { add_name } for comm="binder:269_3" name="globalAlert" scontext=u:r:netd:s0 tcontext=u:object_r:proc_net:s0 tclass=dir permissive=1
[   61.116773] type=1400 audit(1694786183.630:183): avc: denied { create } for comm="binder:269_3" name="globalAlert" scontext=u:r:netd:s0 tcontext=u:object_r:proc_net:s0 tclass=file permissive=1
[   65.082802] binder_alloc: 2519: binder_alloc_buf, no vma
[   65.082821] binder: 449:693 transaction failed 29189/-3, size 664-8 line 3346
[   65.090365] binder: undelivered TRANSACTION_COMPLETE
[   65.090389] binder: undelivered transaction 74113, process died.
[   65.751310] binder: 1070:1562 transaction failed 29189/-22, size 168-0 line 3147
[  166.364112] type=1400 audit(1694786288.886:184): avc: denied { search } for comm="RenderThread" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:priv_app:s0:c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.android.chrome
[  167.463265] type=1400 audit(1694786289.986:185): avc: denied { search } for comm="CrGpuMain" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:priv_app:s0:c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.android.chrome
[  219.618294] type=1400 audit(1694786342.130:186): avc: denied { search } for comm="RenderThread" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:untrusted_app:s0:c79,c256,c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.google.android.inputmethod.latin
[  220.730243] binder: undelivered transaction 115417, process died.
[  220.730260] binder: undelivered transaction 115418, process died.
[  220.730377] binder: undelivered transaction 115416, process died.
[  256.164487] binder: undelivered transaction 123475, process died.
[  258.990404] binder: undelivered TRANSACTION_COMPLETE
[  258.990425] binder: undelivered transaction 125422, process died.
[  258.991565] binder: 449:2607 transaction failed 29189/-22, size 664-8 line 3147
[  258.997150] binder: undelivered transaction 125419, process died.
[  261.465046] binder: undelivered transaction 127452, process died.
[  263.906219] binder: undelivered TRANSACTION_COMPLETE
[  263.906244] binder: undelivered transaction 128528, process died.
[  263.907164] binder: undelivered transaction 128548, process died.
[  284.056113] type=1400 audit(1694786406.580:187): avc: denied { read } for comm="RenderThread" name="u:object_r:vendor_system_public_prop:s0" dev="tmpfs" ino=310 scontext=u:r:permissioncontroller_app:s0:c50,c256,c512,c768 tcontext=u:object_r:vendor_system_public_prop:s0 tclass=file permissive=1 app=com.android.permissioncontroller
[  284.056497] type=1400 audit(1694786406.580:188): avc: denied { open } for comm="RenderThread" path="/dev/__properties__/u:object_r:vendor_system_public_prop:s0" dev="tmpfs" ino=310 scontext=u:r:permissioncontroller_app:s0:c50,c256,c512,c768 tcontext=u:object_r:vendor_system_public_prop:s0 tclass=file permissive=1 app=com.android.permissioncontroller
[  284.057008] type=1400 audit(1694786406.580:189): avc: denied { getattr } for comm="RenderThread" path="/dev/__properties__/u:object_r:vendor_system_public_prop:s0" dev="tmpfs" ino=310 scontext=u:r:permissioncontroller_app:s0:c50,c256,c512,c768 tcontext=u:object_r:vendor_system_public_prop:s0 tclass=file permissive=1 app=com.android.permissioncontroller
[  284.060565] type=1400 audit(1694786406.580:190): avc: denied { map } for comm="RenderThread" path="/dev/__properties__/u:object_r:vendor_system_public_prop:s0" dev="tmpfs" ino=310 scontext=u:r:permissioncontroller_app:s0:c50,c256,c512,c768 tcontext=u:object_r:vendor_system_public_prop:s0 tclass=file permissive=1 app=com.android.permissioncontroller
[  284.064164] type=1400 audit(1694786406.586:191): avc: denied { search } for comm="RenderThread" name="/" dev="mmcblk2p11" ino=2 scontext=u:r:permissioncontroller_app:s0:c50,c256,c512,c768 tcontext=u:object_r:metadata_file:s0 tclass=dir permissive=1 app=com.android.permissioncontroller
[  286.897306] type=1400 audit(1694786409.403:192): avc: denied { getattr } for comm="ThreadPoolForeg" path="/proc/cmdline" dev="proc" ino=4026532094 scontext=u:r:priv_app:s0:c512,c768 tcontext=u:object_r:proc_cmdline:s0 tclass=file permissive=1 app=com.android.chrome
[  296.791006] logd: start watching /data/system/packages.list ...
[  296.796698] logd: ReadPackageList, total packages: 80
[  372.610204] type=1400 audit(1694786495.133:193): avc: denied { read } for comm="binder:179_2" name="wakeup4" dev="sysfs" ino=15010 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=dir permissive=1
[  372.610776] type=1400 audit(1694786495.133:194): avc: denied { open } for comm="binder:179_2" path="/sys/devices/platform/ff740100.rkvdec/wakeup/wakeup4" dev="sysfs" ino=15010 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=dir permissive=1
[  372.611073] type=1400 audit(1694786495.133:195): avc: denied { read } for comm="binder:179_2" name="event_count" dev="sysfs" ino=15017 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[  372.611372] type=1400 audit(1694786495.133:196): avc: denied { open } for comm="binder:179_2" path="/sys/devices/platform/ff740100.rkvdec/wakeup/wakeup4/event_count" dev="sysfs" ino=15017 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[  372.611633] type=1400 audit(1694786495.133:197): avc: denied { getattr } for comm="binder:179_2" path="/sys/devices/platform/ff740100.rkvdec/wakeup/wakeup4/event_count" dev="sysfs" ino=15017 scontext=u:r:system_suspend:s0 tcontext=u:object_r:sysfs:s0 tclass=file permissive=1
[  445.419412] rk_gmac-dwmac ffbd0000.ethernet eth0: Link is Up - 100Mbps/Full - flow control rx/tx
[  445.419483] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[  532.932209] binder_alloc: 3395: binder_alloc_buf, no vma
[  532.932226] binder: 449:1423 transaction failed 29189/-3, size 664-8 line 3346
[  532.934198] binder: undelivered transaction 158437, process died.
[  534.387430] type=1400 audit(1694786656.903:198): avc: denied { ioctl } for comm="Thread-2" path="socket:[53112]" dev="sockfs" ino=53112 ioctlcmd=0x8927 scontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tcontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tclass=udp_socket permissive=1 app=com.waxrain.airplaydmr3
[  534.583938] type=1400 audit(1694786657.106:199): avc: denied { ptrace } for comm="VALIDATE" scontext=u:r:zygote:s0 tcontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tclass=process permissive=1 app=com.waxrain.airplaydmr3
[  534.909611] type=1400 audit(1694786657.433:200): avc: denied { ioctl } for comm="Thread-2" path="socket:[53181]" dev="sockfs" ino=53181 ioctlcmd=0x8927 scontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tcontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tclass=tcp_socket permissive=1 app=com.waxrain.airplaydmr3
[  537.672613] binder_alloc: 3910: binder_alloc_buf, no vma
[  537.672634] binder: 449:1251 transaction failed 29189/-3, size 664-8 line 3346
[  537.676156] binder: release 3910:3957 transaction 162678 out, still active
[  537.676170] binder: undelivered TRANSACTION_COMPLETE
[  537.676179] binder: send failed reply for transaction 162681 to 1070:1757
[  537.680885] binder: 1070:1757 transaction failed 29189/0, size 4-0 line 3078
[  537.680905] binder: send failed reply for transaction 162678, target dead
[  540.968720] binder: undelivered TRANSACTION_COMPLETE
[  540.968738] binder: undelivered transaction 165317, process died.
[  540.968753] binder: undelivered TRANSACTION_COMPLETE
[  540.968758] binder: undelivered transaction 165333, process died.
[  540.968767] binder: undelivered TRANSACTION_COMPLETE
[  540.968771] binder: undelivered transaction 165334, process died.
[  540.969928] binder: undelivered transaction 165358, process died.
[  609.579042] rk_gmac-dwmac ffbd0000.ethernet eth0: Link is Down
[  611.606082] rk_gmac-dwmac ffbd0000.ethernet eth0: Link is Up - 100Mbps/Full - flow control rx/tx
[  659.232315] rk_gmac-dwmac ffbd0000.ethernet eth0: Link is Down
[  660.249292] rk_gmac-dwmac ffbd0000.ethernet eth0: Link is Up - 100Mbps/Full - flow control rx/tx
[  716.118883] type=1400 audit(1694786838.640:201): avc: denied { ioctl } for comm="Thread-2" path="socket:[56284]" dev="sockfs" ino=56284 ioctlcmd=0x8927 scontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tcontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tclass=udp_socket permissive=1 app=com.waxrain.airplaydmr3
[  732.244502] type=1400 audit(1694786854.766:202): avc: denied { ioctl } for comm="mDNSResponder_m" path="socket:[58797]" dev="sockfs" ino=58797 ioctlcmd=0x8914 scontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tcontext=u:r:untrusted_app_27:s0:c78,c256,c512,c768 tclass=udp_socket permissive=1 app=com.waxrain.airplaydmr3
[  864.852684] Exit. P2P interface stopped
[  864.965876] aicwf_sdio mmc1:80e4:1 wlan0: CLOSE
[  864.968324] rwnx_send_msg1 (9)MM_REMOVE_IF_CFM reqcfm:1 in_irq:0 in_softirq:0 in_atomic:0\x0d
[  864.974802] ieee80211 phy0: HT supp 1, VHT supp 1, HE supp 1
[  864.988397] init: Service 'wpa_supplicant' (pid 699) exited with status 0 oneshot service took 840.718994 seconds in background
[  864.988506] init: Sending signal 9 to service 'wpa_supplicant' (pid 699) process group...
[  864.989216] libprocessgroup: Successfully killed process cgroup uid 0 pid 699 in 0ms
[  920.174973] binder: undelivered transaction 187404, process died.
[  920.174988] binder: undelivered transaction 190369, process died.
[  920.174994] binder: undelivered transaction 190453, process died.
[  920.174998] binder: undelivered transaction 201030, process died.
[  920.175002] binder: undelivered transaction 201122, process died.
[  920.175006] binder: undelivered transaction 207197, process died.
[  920.175011] binder: undelivered transaction 207274, process died.
[  920.175015] binder: undelivered transaction 218512, process died.
[  920.175019] binder: undelivered transaction 218597, process died.
[  920.175170] binder: undelivered transaction 187317, process died.
```

## Android build.prop
```
# cat /vendor/build.prop
####################################
# from generate-common-build-props
# These properties identify this partition image.
####################################
ro.product.vendor.brand=Android
ro.product.vendor.device=rockchip001
ro.product.vendor.manufacturer=RockChip
ro.product.vendor.model=HK1 RBOX K8
ro.product.vendor.name=rockchip001
ro.vendor.product.cpu.abilist=arm64-v8a,armeabi-v7a,armeabi
ro.vendor.product.cpu.abilist32=armeabi-v7a,armeabi
ro.vendor.product.cpu.abilist64=arm64-v8a
ro.vendor.build.date=Fri Sep 15 21:55:44 CST 2023
ro.vendor.build.date.utc=1694786144
ro.vendor.build.fingerprint=Android/rockchip001/rockchip001:13/TQ2A.230305.008.F1/user09152155:user/release-keys
ro.vendor.build.id=TQ2A.230305.008.F1
ro.vendor.build.tags=release-keys
ro.vendor.build.type=user
ro.vendor.build.version.incremental=eng.user.20230915.215803
ro.vendor.build.version.release=13
ro.vendor.build.version.release_or_codename=13
ro.vendor.build.version.sdk=33
####################################
# from out/target/product/rockchip001/obj/ETC/android_info_prop_intermediates/android_info.prop
####################################
####################################
# from variable ADDITIONAL_VENDOR_PROPERTIES
####################################
ro.vndk.version=33
ro.bionic.arch=arm64
ro.bionic.cpu_variant=cortex-a53
ro.bionic.2nd_arch=arm
ro.bionic.2nd_cpu_variant=cortex-a53
persist.sys.dalvik.vm.lib.2=libart.so
dalvik.vm.isa.arm64.variant=cortex-a53
dalvik.vm.isa.arm64.features=default
dalvik.vm.isa.arm.variant=cortex-a53
dalvik.vm.isa.arm.features=default
ro.minui.default_rotation=ROTATION_NONE
ro.minui.overscan_percent=2
ro.minui.pixel_format=RGBX_8888
ro.boot.dynamic_partitions=true
ro.product.first_api_level=33
ro.vendor.build.security_patch=2023-03-05
ro.product.board=RK3528
ro.board.platform=rk3528
ro.hwui.use_vulkan=
####################################
# from variable PRODUCT_VENDOR_PROPERTIES
####################################
ro.zygote=zygote64_32
ro.soc.manufacturer=Rockchip
# Removed by post_process_props.py because overridden by ro.zygote=zygote64_32
#ro.zygote?=zygote32
ro.logd.size.stats=64K
log.tag.stats_log=I
external_storage.projid.enabled=1
external_storage.casefold.enabled=1
external_storage.sdcardfs.enabled=0
drm.service.enabled=true
dalvik.vm.heapstartsize=8m
dalvik.vm.heapgrowthlimit=256m
dalvik.vm.heapsize=512m
dalvik.vm.heaptargetutilization=0.75
dalvik.vm.heapminfree=512k
dalvik.vm.heapmaxfree=8m
####################################
# from variable PRODUCT_DEFAULT_PROPERTY_OVERRIDES
####################################
ro.oem_unlock_supported=1
ro.enable_boot_charger_mode=0
ro.board.platform=rk3528
ro.flash_img.enable=true
ro.opengles.version=131072
ro.hwui.drop_shadow_cache_size=4.0
ro.hwui.gradient_cache_size=0.8
ro.hwui.layer_cache_size=32.0
ro.hwui.path_cache_size=24.0
ro.hwui.text_large_cache_width=2048
ro.hwui.text_large_cache_height=1024
ro.hwui.text_small_cache_width=1024
ro.hwui.text_small_cache_height=512
ro.hwui.texture_cache_flushrate=0.4
ro.hwui.texture_cache_size=72.0
debug.hwui.use_partial_updates=false
####################################
# from variable PRODUCT_PROPERTY_OVERRIDES
####################################
ro.rksdk.version=ANDROID13_RKR5
dalvik.vm.boot-dex2oat-threads=4
dalvik.vm.dex2oat-threads=4
persist.sys.media.avsync=true
persist.sys.audio.enforce_safevolume=false
ro.rk.bt_enable=true
persist.bluetooth.btsnoopenable=false
persist.bluetooth.btsnooppath=/sdcard/btsnoop_hci.cfa
persist.bluetooth.btsnoopsize=0xffff
persist.bluetooth.rtkcoex=true
bluetooth.enable_timeout_ms=11000
ro.vendor.mpp_buf_type=1
ro.hardware.egl=mali
persist.sys.strictmode.visual=false
ro.rk.flash_enable=true
ro.rk.hdmi_enable=true
ro.factory.hasUMS=false
testing.mediascanner.skiplist=/mnt/shell/emulated/Android/
ro.factory.hasGPS=false
ro.rk.ethernet_settings=true
ro.factory.storage_suppntfs=true
ro.factory.without_battery=false
ro.rk.screenoff_time=2147483647
ro.incremental.enable=yes
ro.boot.noril=true
keyguard.no_require_sim=true
ro.com.android.dataroaming=true
ril.function.dataonly=1
ro.config.enable.remotecontrol=false
ro.config.enable.skipverify=true
ro.hdmi.device_type=4
ro.vendor.hdmi_settings=true
ro.vendor.udisk.visible=true
ro.safemode.disabled=true
ro.rk.screenshot_enable=true
ro.rk.hdmi_enable=true
sys.status.hidebar_enable=false
persist.sys.zram_enabled=1
persist.sys.sf.color_saturation=1.0
ro.prop.cmcc_split_cnt=4_6
persist.sys.fuse.passthrough.enable=true
ro.vendor.rk_sdk=1
sys.video.afbc=1
vendor.gralloc.disable_afbc=1
vendor.gralloc.no_afbc_for_fb_target_layer=1
wifi.interface=wlan0
ro.audio.monitorOrientation=true
vendor.hwc.compose_policy=1
sf.power.control=2073600
ro.tether.denied=false
sys.resolution.changed=false
ro.product.usbfactory=rockchip_usb
wifi.supplicant_scan_interval=15
ro.kernel.android.checkjni=0
ro.vendor.nrdp.modelgroup=NEXUSPLAYERFUGU
vendor.hwc.device.primary=HDMI-A,TV
vendor.hwc.video_buf_cache_max_size=29491199
persist.vendor.framebuffer.main=1920x1080@60
persist.vendor.framebuffer.aux=1920x1080@60
ro.vendor.sdkversion=rk3528_ANDROID13.0_BOX_V1.0
rt_vtunnel_enable=1
rt_retriever_max_size=4096
ro.com.google.clientidbase=android-rockchip-tv
persist.sys.locale=en-US
persist.sys.timezone=America/New_York
ro.sf.lcd_density=240
ro.control_privapp_permissions=log
ro.target.product=box
persist.sys.bootvideo.enable=true
persist.sys.bootvideo.showtime=-2
ro.config.wallpaper=vendor/etc/default_wallpaper.png
persist.vendor.rootservice=1
sys.customapp.allapp_package=com.hisight.mslauncher
sys.customapp.allapp_class=com.hisight.mslauncher.MoreAppsActivity
# end of file
```
